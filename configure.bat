@echo off

pushd %~dp0build
windows\premake5.exe --file=premake5.lua --os=windows vs2015 %*
windows\premake5.exe --file=premake5.lua --os=windows vs2013 %*
popd

:: prompt for press any key to continue if we're not running from a command prompt
@echo %cmdcmdline% | findstr /l "\"\"" >NUL 2>&1
if %errorlevel% EQU 0 pause
