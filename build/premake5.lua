------------------------------------------------------------------------------------------------------------------------
-- premake script to generate squirrel bind solution
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- actions
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- packages up everything in the repository for uploading and releasing
------------------------------------------------------------------------------------------------------------------------
newaction {
  trigger = "package",
  description = "Build a release package for the project",
  execute = function ()
    dofile "package.lua"
  end
}

------------------------------------------------------------------------------------------------------------------------
-- options
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- specify the location of squirrel
------------------------------------------------------------------------------------------------------------------------
newoption {
  trigger     = "squirrel",
  description = "Specify a location for squirrel.",
  value       = "path",
}

-- default to the thirdparty submodule for squirrel
--
if not _OPTIONS["squirrel"] then
  _OPTIONS["squirrel"] = path.join("..", "thirdparty", "squirrel")
end

------------------------------------------------------------------------------------------------------------------------
-- specify the location of googletest
------------------------------------------------------------------------------------------------------------------------
newoption {
  trigger = "gtest",
  description = "Specify a location for googletest.",
  value   = "path",
}

-- ensure googletest path is absolute if it was set
--
if not _OPTIONS["gtest"] then
  _OPTIONS["gtest"] = path.join("..", "thirdparty", "googletest",  "googletest")
end

------------------------------------------------------------------------------------------------------------------------
-- specify the location of googlemock
------------------------------------------------------------------------------------------------------------------------
newoption {
  trigger = "gmock",
  description = "Specify a location for googlemock.",
  value   = "path",
}

-- ensure googlemock path is absolute if it was set
--
if not _OPTIONS["gmock"] then
  _OPTIONS["gmock"] = path.join("..", "thirdparty", "googletest",  "googlemock")
end

------------------------------------------------------------------------------------------------------------------------
-- premake action dependent variable setup 
------------------------------------------------------------------------------------------------------------------------
actions = { }

windows_platforms = { "Win32-x86_32", "Win32-x86_64", }
windows_configurations = { "debug-mbcs", "release-mbcs", "debug-unicode", "release-unicode", }

linux_platforms = {  "clang-x86_32", "clang-x86_64", "gcc-x86_32", "gcc-x86_64", }
linux_configurations = { "debug", "release" }

-- don't need platforms or configurations for package action
--
actions.package = {
  platforms = {},
  configurations = {},
}

-- supported visual studio actions
--
actions.vs2015 = {
  platforms = windows_platforms,
  configurations = windows_configurations,
}
actions.vs2013 = actions.vs2015

-- supported linux actions
--
actions.gmake = {
  platforms = linux_platforms,
  configurations = linux_configurations,
}

if not _ACTION then
  return
end

if not actions[_ACTION] then
  error(string.format("Premake5 action '%s' is not supported by this library", _ACTION))
end

------------------------------------------------------------------------------------------------------------------------
-- sqbind solution
------------------------------------------------------------------------------------------------------------------------
workspace "sqbind"
  -- setup the ouput location of the project
  --
  location(_ACTION)

  -- setup the different build configurations
  --
  platforms(actions[_ACTION].platforms)
  configurations(actions[_ACTION].configurations)

  -- set a solution wide objdir relative to this file ie. "../lib/gmake/squirrel/windows/x86"
  --
  objdir(path.join("..", "obj", _ACTION, "%{cfg.system}"))

  -- set a solution wide libdir relative to this file ie. "../lib/gmake/windows/x86"
  --
  libdirs {
    path.join("..", "lib", _ACTION, "%{cfg.platform}", "%{cfg.buildcfg}"),
  }

  -- set maximum warning level and warnings as errors
  --
  flags { "FatalWarnings", "c++11", }
  symbols "on"
  warnings "Extra"

  -- add the build files to every project
  --
  files { "../build/**.lua", }

  -- by default file paths starting with .. are no longer grouped in the workspace
  --
  vpaths {
    ["*"] = "../**",
  }

  -- set the default startup project
  --
  startproject "test-sqbind"

  -- setup some platform related defines
  --
  filter "platforms:*_32"
    architecture "x86"

  filter "platforms:*_64"
    architecture "x86_64"
    defines{ "_SQ64", }

  filter "platforms:Win*"
    system "windows"
	defines{ "_CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES", }

  filter "platforms:clang* or gcc*"
    system "linux"

  filter "platforms:clang*"
    toolset "clang"

  filter "platforms:gcc*"
    toolset "gcc"

  -- setup the configuration defines
  --
  filter "configurations:debug*"
    optimize "Off"
    defines{ "_DEBUG", "DEBUG", }

  filter "configurations:release*"
    optimize "Full"
    defines{ "_RELEASE", "NDEBUG", }

  filter "configurations:*-mbcs"
    characterset "MBCS"

  filter "configurations:*-unicode"
    characterset "Unicode"

  configuration { }

  -- squirrel
  --
  project "squirrel"
    language "C++"
    kind "StaticLib"

    files {
      path.join(_OPTIONS["squirrel"], "include", "sqconfig.h"),
      path.join(_OPTIONS["squirrel"], "include", "squirrel.h"),
      path.join(_OPTIONS["squirrel"], "squirrel", "**.cpp"),
      path.join(_OPTIONS["squirrel"], "squirrel", "**.h"),
    }

    targetdir(path.join("..", "lib", _ACTION, "%{cfg.system}", "%{cfg.platform}", "%{cfg.buildcfg}"))

    includedirs {
      path.join(_OPTIONS["squirrel"], "include"),
    }

    filter "platforms:Win*"
      disablewarnings {
        "4100", -- unreferenced formal parameter
        "4127", -- conditional expression is constant
        "4244", -- conversion from '' to '', possible loss of data
        "4456", -- declaration of '' hides previous local declaration
        "4457", -- declaration of '' hides function parameter
        "4458", -- declaration of '' hides class member
        "4611", -- interaction between '' and C++ object destruction is non-portable
        "4702", -- unreachable code
        "4706", -- assignment within conditional expression
        "4996", -- This function or variable may be unsafe.
      }

    filter "platforms:clang*"
      disablewarnings { "unused-parameter", "unused-variable", "missing-field-initializers" }

  -- squirrel standard library
  --
  project "sqstdlib"
    language "C++"
    kind "StaticLib"

    files {
      path.join(_OPTIONS["squirrel"], "include", "sqstdaux.h"),
      path.join(_OPTIONS["squirrel"], "include", "sqstdblob.h"),
      path.join(_OPTIONS["squirrel"], "include", "sqstdio.h"),
      path.join(_OPTIONS["squirrel"], "include", "sqstdmath.h"),
      path.join(_OPTIONS["squirrel"], "include", "sqstdstring.h"),
      path.join(_OPTIONS["squirrel"], "include", "sqstdsystem.h"),
      path.join(_OPTIONS["squirrel"], "sqstdlib", "**.cpp"),
      path.join(_OPTIONS["squirrel"], "sqstdlib", "**.h"),
    }

    targetdir(path.join("..", "lib", _ACTION, "%{cfg.system}", "%{cfg.platform}", "%{cfg.buildcfg}"))

    includedirs {
      path.join(_OPTIONS["squirrel"], "include"),
    }

    filter "platforms:Win*"
      disablewarnings {
        "4100", -- unreferenced formal parameter
        "4611", -- interaction between '' and C++ object destruction is non-portable
        "4702", -- unreachable code
        "4706", -- assignment within conditional expression
        "4996", -- This function or variable may be unsafe.
      }

    filter "platforms:clang*"
      disablewarnings { "unused-parameter", "missing-field-initializers", "format" }

  ----------------------------------------------------------------------------------------------------------------------
  -- sqbind project
  ----------------------------------------------------------------------------------------------------------------------
  project "sqbind"
    language "C++"
    kind "StaticLib"

    files {
      path.join("..", "include", "sqbind", "**.h"),
      path.join("..", "include", "sqbind", "**.inl"),
      path.join("..", "scripts", "**.nut"),
      path.join("..", "src", "**.cpp"),
      path.join("..", "src", "**.h"),
    }


    pchheader("sqbPrecompiled.h")
    pchsource(path.join("..", "src", "sqbPrecompiled.cpp"))
    includedirs { path.join("..", "src"), }

    targetdir(path.join("..", "lib", _ACTION, "%{cfg.system}", "%{cfg.platform}", "%{cfg.buildcfg}"))

    includedirs {
      path.join("..", "include"),
      path.join(_OPTIONS["squirrel"], "include"),
    }

    defines{ "SQBIND_STATIC", }

    links { "squirrel", }

  ----------------------------------------------------------------------------------------------------------------------
  -- sqbind test
  ----------------------------------------------------------------------------------------------------------------------
  project "test-sqbind"
    language "C++"
    kind "ConsoleApp"

    files {
      path.join("..", "test", "**.h"),
      path.join("..", "test", "**.inl"),
      path.join("..", "test", "**.cpp"),
      path.join(_OPTIONS["gtest"], "include", "**.h"),
      path.join(_OPTIONS["gtest"], "src", "gtest-all.cc"),
      path.join(_OPTIONS["gmock"], "include", "**.h"),
      path.join(_OPTIONS["gmock"], "src", "gmock-all.cc"),
      path.join(_OPTIONS["gmock"], "src", "gmock_main.cc"),
    }

    targetdir(path.join("..", "bin", _ACTION, "%{cfg.system}", "%{cfg.platform}", "%{cfg.buildcfg}"))

    includedirs {
      path.join("..", "test"),
      path.join("..", "include"),
      path.join(_OPTIONS["squirrel"], "include"),
      path.join(_OPTIONS["gtest"], "include"),
      path.join(_OPTIONS["gtest"]),
      path.join(_OPTIONS["gmock"], "include"),
      path.join(_OPTIONS["gmock"]),
    }

    defines{ "SQBIND_STATIC", }

    links {
      "sqbind",
      "squirrel",
      "sqstdlib",
      "pthread", -- for googletest
    }

    filter "action:vs*"
      forceincludes {
        "Precompiled.h",
      }

      pchheader("Precompiled.h")
      pchsource(path.join("..", "test", "Precompiled.cpp"))

  -- sqbind examples
  --
  project "sqbindexamples"
    language "C++"
    kind "ConsoleApp"

    files {
      path.join("..", "examples", "*.h"),
      path.join("..", "examples", "*.inl"),
      path.join("..", "examples", "*.cpp"),
    }

    targetdir(path.join("..", "bin", "%{cfg.system}", "%{cfg.platform}", "%{cfg.buildcfg}"))

    includedirs {
      path.join("..", "include"),
      path.join(_OPTIONS["squirrel"], "include"),
    }

    defines {
      "SQBIND_STATIC",
    }

    links {
      "sqbind",
      "squirrel",
      "sqstdlib",
    }
