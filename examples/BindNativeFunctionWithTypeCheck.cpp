#include <sqbind/sqbBind.h>

// This function prints type check passed to stdout if it is successfully called.
//
SQInteger NativeFunctionWithTypeCheck(HSQUIRRELVM SQBIND_UNUSED(vm))
{
  scprintf(_SC("type check passed"));
  return 0;
}

// This will bind the above function to the root table of vm.
//
void BindNativeFunctionWithTypeCheck(HSQUIRRELVM vm)
{
  sq_pushroottable(vm);

  sqb::FunctionOptions options;

  // All functions have their environment as the first parameter.
  // This type mask and param count indicate the function should only
  // accept any environment and an integer as function arguments.
  //
  options.TypeMask(_SC(".i"));
  options.ParamCheckCount(2);

  // Bind the function.
  //
  sqb::Bind::BindNativeFunction(vm, -1, &NativeFunctionWithTypeCheck, _SC("NativeFunctionWithTypeCheck"), options);

  sq_poptop(vm);
}