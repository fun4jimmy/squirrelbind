#include <sqbind/sqbBind.h>

// this function will return the the sum of the two int32_t arguments
//
int32_t Function(int32_t a, int32_t b)
{
  int32_t result = a + b;
  return result;
}

// this will bind the above function to the root table of vm
//
void BindFunction(HSQUIRRELVM vm)
{
  sq_pushroottable(vm);

  // bind the function
  //
  sqb::Bind::BindFunction(vm, -1, &Function, _SC("Function"));

  sq_poptop(vm);
}