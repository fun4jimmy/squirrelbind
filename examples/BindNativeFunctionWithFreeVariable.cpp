#include <sqbind/sqbBind.h>

// this function will return the value of the bound free variable
//
SQInteger NativeFunctionWithFreeVariable(HSQUIRRELVM vm)
{
  SQInteger top = sq_gettop(vm);
  SQInteger freeVariable = 0;
  sq_getinteger(vm, top, &freeVariable);
  sq_pushinteger(vm, freeVariable);
  return 1;
}

// this will bind the above function to the root table of vm
//
void BindNativeFunctionWithFreeVariable(HSQUIRRELVM vm)
{
  sq_pushroottable(vm);

  // this integer will be bound as a free variable
  //
  sq_pushinteger(vm, 10);

  // bind the function
  //
  sqb::Bind::BindNativeFunction(
    vm,
    -2,
    &NativeFunctionWithFreeVariable,
    _SC("NativeFunctionWithFreeVariable"),
    sqb::FunctionOptions()
      .FreeVariables(1));

  sq_poptop(vm);
}