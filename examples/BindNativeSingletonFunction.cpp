#include <sqbind/sqbBind.h>

// declare a basic singleton class
//
class NativeFunctionSingleton
{
public:
  // this function returns the number of arguments it is called with
  //
  SQInteger Function(HSQUIRRELVM vm)
  {
    return sq_gettop(vm);
  }

  static NativeFunctionSingleton s_instance;
};

NativeFunctionSingleton NativeFunctionSingleton::s_instance;

// this will bind the above function to the root table of vm
//
void BindNativeSingletonFunction(HSQUIRRELVM vm)
{
  sq_pushroottable(vm);

  // bind the native singleton function
  //
  sqb::Bind::BindNativeSingletonFunction(vm, -1, &NativeFunctionSingleton::s_instance, &NativeFunctionSingleton::Function, _SC("NativeSingletonFunction"));

  sq_poptop(vm);
}