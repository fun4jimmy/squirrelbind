#include <sqbind/sqbBind.h>

// this function will return the number of arguments it is called with
//
SQInteger NativeFunction(HSQUIRRELVM vm)
{
  SQInteger top = sq_gettop(vm);
  sq_pushinteger(vm, top);
  return 1;
}

// this will bind the above function to the root table of vm
//
void BindNativeFunction(HSQUIRRELVM vm)
{
  sq_pushroottable(vm);

  // bind the function
  //
  sqb::Bind::BindNativeFunction(vm, -1, &NativeFunction, _SC("NativeFunction"));

  sq_poptop(vm);
}