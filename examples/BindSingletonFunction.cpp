#include <sqbind/sqbBind.h>

// declare a basic singleton class
//
class Singleton
{
public:
  // this function will return the sum of its two int32_t arguments
  //
  int32_t Function(int32_t a, int32_t b)
  {
    return a + b;
  }

  static Singleton s_instance;
};

Singleton Singleton::s_instance;

// this will bind the above function to the root table of vm
//
void BindSingletonFunction(HSQUIRRELVM vm)
{
  sq_pushroottable(vm);

  // bind the singleton function
  //
  sqb::Bind::BindSingletonFunction(vm, -1, &Singleton::s_instance, &Singleton::Function, _SC("SingletonFunction"));

  sq_poptop(vm);
}