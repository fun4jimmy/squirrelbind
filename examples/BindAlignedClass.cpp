#include <math.h>

#include <sqbind/sqbBindMacros.h>
#include <sqbind/sqbClassDefinition.h>

#if defined(SQBIND_COMPILER_MSVC)
# define PRE_ALIGN(X) __declspec(align(X))
# define POST_ALIGN(X)
#elif defined(SQBIND_COMPILER_GCC) || defined(SQBIND_COMPILER_CLANG)
# define PRE_ALIGN(X)
# define POST_ALIGN(X) __attribute__((__aligned__(X)))
#else 
# define PRE_ALIGN(X)
# define POST_ALIGN(X)
#endif

// class to be bound with auto release
//
PRE_ALIGN(16)
struct AlignedVector3
{
  float x;
  float y;
  float z;
  float w;
} POST_ALIGN(16);


// make sure the type is aligned
//
SQBIND_DECLARE_CLASS(AlignedVector3);

// this will bind the above class to the root table of vm
//
void BindAlignedClass(HSQUIRRELVM vm)
{
  sq_pushroottable(vm);

  // register the Factory class that can make IUnknowns.
  //
  sqb::ClassDefinition<AlignedVector3>(vm, -1, _SC("AlignedVector3"))
    .Variable(&AlignedVector3::x, _SC("x"))
    .Variable(&AlignedVector3::y, _SC("y"))
    .Variable(&AlignedVector3::z, _SC("z"))
    .Variable(&AlignedVector3::w, _SC("w"));

  sq_poptop(vm);
}
