#include <sqbind/sqbBind.h>

// declare a class
//
class Klass
{
public:
  // this function will return the sum of its two int32_t arguments
  //
  int32_t Function(int32_t a, int32_t b)
  {
    return a + b;
  }
};

// this will bind the above function to the root table of vm
//
void BindClassFunction(HSQUIRRELVM vm)
{
  sq_newclass(vm, SQFalse);

  // bind the new class to the root table
  //
  sq_pushroottable(vm);
  sq_pushstring(vm, _SC("Klass"), -1);
  sq_push(vm, -3);
  sq_newslot(vm, -3, SQFalse);
  sq_poptop(vm);

  // bind the class function
  //
  sqb::Bind::BindClassFunction<Klass>(vm, -1, &Klass::Function, _SC("Function"));

  // pop the new class from the stack
  //
  sq_poptop(vm);
}