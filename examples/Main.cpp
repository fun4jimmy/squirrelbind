#include <squirrel.h>
#include <sqbind/SquirrelBind.h>

extern void BindFunction(HSQUIRRELVM vm);

extern void BindNativeFunction(HSQUIRRELVM vm);
extern void BindNativeFunctionWithFreeVariable(HSQUIRRELVM vm);
extern void BindNativeFunctionWithTypeCheck(HSQUIRRELVM vm);

extern void BindClassFunction(HSQUIRRELVM vm);

extern void BindNativeClassFunction(HSQUIRRELVM vm);

extern void BindSingletonFunction(HSQUIRRELVM vm);

extern void BindNativeSingletonFunction(HSQUIRRELVM vm);

extern void Vector2ClassDefinition(HSQUIRRELVM vm);

extern void BindAutoReleaseClass(HSQUIRRELVM vm);

extern void BindAlignedClass(HSQUIRRELVM vm);

int main(int SQBIND_UNUSED(argc), char **SQBIND_UNUSED(argv))
{
  HSQUIRRELVM vm = sq_open(1024);

  BindFunction(vm);

  BindNativeFunction(vm);
  BindNativeFunctionWithFreeVariable(vm);
  BindNativeFunctionWithTypeCheck(vm);

  BindClassFunction(vm);

  BindNativeClassFunction(vm);

  BindSingletonFunction(vm);

  BindNativeSingletonFunction(vm);

  Vector2ClassDefinition(vm);

  BindAutoReleaseClass(vm);

  sq_close(vm);

  return 0;
}