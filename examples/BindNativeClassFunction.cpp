#include <sqbind/sqbBind.h>

// declare a class
//
class NativeFunctionClass
{
public:
  // this function will return the number of arguments it is called with
  //
  SQInteger Function(HSQUIRRELVM vm)
  {
    SQInteger top = sq_gettop(vm);
    return top;
  }
};

// this will bind the above function to the root table of vm
//
void BindNativeClassFunction(HSQUIRRELVM vm)
{
  sq_newclass(vm, SQFalse);

  // bind the new class to the root table
  //
  sq_pushroottable(vm);
  sq_pushstring(vm, _SC("NativeFunctionClass"), -1);
  sq_push(vm, -3);
  sq_newslot(vm, -3, SQFalse);
  sq_poptop(vm);

  // bind the class function
  //
  sqb::Bind::BindNativeClassFunction<NativeFunctionClass>(vm, -1, &NativeFunctionClass::Function, _SC("Function"));

  // pop the new class from the stack
  //
  sq_poptop(vm);
}