#include <math.h>

#include <sqbind/sqbBindMacros.h>
#include <sqbind/sqbClassDefinition.h>

class Vector2
{
public:
  static Vector2 kZero;
  static Vector2 kXAxis;
  static Vector2 kYAxis;
  static Vector2 kOne;

  float x;
  float y;

  Vector2() : x(0), y(0) {}
  Vector2(float _x, float _y) : x(_x), y(_y) {}

  float Length() const
  {
    float lengthSquared = (x * x) + (y * y);
    float length = sqrtf(lengthSquared);
    return length;
  }

  Vector2 operator + (const Vector2& rhs) const
  {
    Vector2 result;
    result.x = x + rhs.x;
    result.y = y + rhs.y;
    return result;
  }
};

Vector2 Vector2::kZero  = Vector2(0.0f, 0.0f);
Vector2 Vector2::kXAxis = Vector2(1.0f, 0.0f);
Vector2 Vector2::kYAxis = Vector2(0.0f, 1.0f);
Vector2 Vector2::kOne   = Vector2(1.0f, 1.0f);

// declares sqb::TypeInfo, sqb::Push, sqb::Match and sqb::Get for the Vector2.
//
SQBIND_DECLARE_CLASS(Vector2);

// this will bind the above class to the root table of vm
//
void Vector2ClassDefinition(HSQUIRRELVM vm)
{
  sq_pushroottable(vm);

  sqb::ClassDefinition<Vector2>(vm, -1, _SC("Vector2"))
    .StaticVariable(Vector2::kZero, _SC("kZero"))
    .StaticVariable(Vector2::kXAxis, _SC("kXAxis"))
    .StaticVariable(Vector2::kYAxis, _SC("kYAxis"))
    .StaticVariable(Vector2::kOne, _SC("kOne"))
    .Variable(&Vector2::x, _SC("x"))
    .Variable(&Vector2::y, _SC("y"))
    .ClassFunction(&Vector2::Length, _SC("length"))
    .ClassFunction(&Vector2::operator +, _SC("_add"));

  sq_poptop(vm);
}
