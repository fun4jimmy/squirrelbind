# squirrelbind 2.1 #
--------------------
 - add squirrel 3.1 as a thirdparty submodule.
 - add googletest 1.7.0 as a thirdparty submodule, not using the 1.7.0 release tag to get the combined googlemock and googletest repository.
 - update to premake 5.0
 - added per character set configurations for windows (MBCS and Unicode).
 - added support for Visual Studio 2015
 - removed old premake 4.x 'package' action
 - added premake5.exe and configure.bat for easy project generation on windows platforms
 - fix conversion from 'double' to 'float' warning in examples\ClassDefinition.cpp
 - fix compile errors when building with SQUNICODE defined.
 - fix memory corruption bugs when SQUNICODE was defined, sq_getscratchpad parameter is size in bytes not SQChar count.
 - fix warning returning address of local variable or temporary in sqbSquirrelFunction.
 - remove windows specific _CRT_SECURE_NO_WARNINGS define from TestVersion.cpp by replacing use of scsprintf with std::string.
 - add precompiled header to test-sqbind project for faster compilation times.
 - convert changes.txt to CHANGES.md for better display on bitbucket.
 - update Doxyfile to Doxygen version 1.8.11
 - fix Doxygen warnings
 - Match<T> and Get<T> implementations are no longer specialised using macros as they are the same for every type.
 - test for binding const member functions was using non-const mock method
 - fix arguments to vsnprintf in strict ansi gnuc builds
 - fix clang error enumeration value 'kIterationInvalid' not handled in switch
 - fix clang error use 'template' keyword to treat 'Compare' as a dependent template name in test/TypeFixture
 - fix clang error comparison of integers of different signs in TestBindNativeClassFunction.cpp
 - added Push, Match and Get overloads for long long and unsigned long long to fix an undefined reference for non MSVC 64-bit builds when binding SQInteger and SQUnsignedInteger.
 - disabled specific warnings for squirrel and sqstdlib so they build with no warnings with clang
 - fix formatted string error in sqb::StackHandler::ThrowError on linux.
 - disabled specific warnings for squirrel and sqstdlib so they build with no warnings with MSVC140.

# squirrelbind 2.0 #
--------------------
 - replace auto generated ParamCheckHelper, Call and ReturnSpecialisation with hand written C++11 variadic template versions.
 - remove nullptr emulation for older compilers.
 - use keyword override.
 - remove TypeTraits.h and use c++11 standard library type_traits classes instead. There is a bug in Visual Studio 2013 with std::is_copy_constructible and std::is_copy_assignable that means a macro is still required.
 - delete implicitly-declared copy constructor and assignment constructor where appropriate rather than just declare them privately.
 - replace auto generated tests with hand written tests for binding functions with no arguments and one argument now variadic templates are being used.
 - use keyword auto where possible
 - add move constructor and move assignment operator for TableIterator.
 - add move constructor and move assignment operator for ScopedArrayAccess.
 - add move constructor and move assignment operator for ScopedTableAccess.
 - premake generated projects now always have the same uuid.
 - improved project file filters
 
# squirrelbind 1.03 #
---------------------
 - change autogen files to always have the current year in the copyright comment.
 - fix visual studio compiler error in assert preprocessor conditions (sqbAssert.h line 153)
 - remove debug print from autogen script file.
 - add constructor and destructor definition to all mock classes in unittests to speed up test project compilation.
   See http://code.google.com/p/googlemock/wiki/V1_6_CookBook#Making_the_Compilation_Faster for more information.
 - fix bug required gcc version to use diagnostic pragmas within function scope.
 - fix bug move clang __has_feature(cxx_nullptr) inside the clang macro so other compilers don't parse it.
 - fix MSVC10 compile error in sqbAssert.h - thanks luisjimen (Fixes Issue 1)

# squirrelbind 1.02 #
---------------------
 - added defines for the current compiler SQBIND_COMPILER_MSVC, SQBIND_COMPILER_GCC or SQBIND_COMPILER_CLANG
 - added define SQBIND_COMPILER_VERSION for current compiler version number.
 - fixed clang compiler errors.
 - fix bug with the precompiled header specified in squirrelbind project.
 - better handling of the squirrelbind version number when packaging a release.

# squirrelbind 1.01 #
---------------------
 - refactor the code so it compiles with gcc 4.4, mainly changing a lot of syntax that is accepted in visual studio but not the stricter gcc compiler.
   tested with the Google Native Client Pepper 22 toolchain.
 - define SQBIND_C0X_NULLPTR_SUPPORTED renamed to SQBIND_CPP0X_NULLPTR_SUPPORTED and always defined as either 1 or 0 to enable/disable.
 - fixed template argument order for ScopedTableAccess::GetSlot and ScopedTableAccess::RawGetSlot.
 - added static SquirrelObject::kNullObject to save constructing a null SquirrelObject all the time.
 - added convienience function SquirrelObject::IsNull to check if a SquirrelObject is referencing a null object.
 - added convienience function SquirrelObject::IsBool to check if a SquirrelObject is referencing a boolean value.
 - added convienience function SquirrelObject::IsFloat to check if a SquirrelObject is referencing a float value.
 - added convienience function SquirrelObject::IsInteger to check if a SquirrelObject is referencing an integer value.
 - added convienience function SquirrelObject::IsString to check if a SquirrelObject is referencing a string value.
 - added convienience function SquirrelObject::IsTable to check if a SquirrelObject is referencing a table value.
 - added convienience function SquirrelObject::AsBool get the referenced object as a boolean value.
 - added convienience function SquirrelObject::AsFloat get the referenced object as a float value.
 - added convienience function SquirrelObject::AsInteger get the referenced object as an integer value.
 - added convienience function SquirrelObject::AsString get the referenced object as a string value.
 - added a constructor to SquirrelVM that also initialises the vm.
 - added SquirrelVM::IsInitialised to check if the vm has been initialised already.
 - added overloads to ScopedTableAccess member functions SlotExists, RawSlotExists, GetSlotType, RawGetSlotType, NewSlot, GetSlot, RawGetSlot, SetSlot and RawSetSlot so the key can be a specific length string not just a null terminated string.
 - added additional functions to ScopedTableAccess NewSlotNewArray, NewSlotNewTable, SetSlotNewArray, SetSlotNewTable, RawSetSlotNewArray and RawSetSlotNewTable for dealing with tables and arrays.
 - changed the signature of TableIterator functions GetKey and GetValue as returning by reference is impossible for certain types.
 - added convienience function SquirrelObject::GetTypeName for error messages.
 - fixed SquirrelVM::ExecuteBuffer popping too many arguments when the call to sq_call fails.
 - provide an overloads for SquirrelObject DeleteSlot and RawDeleteSlot rather than default arguments so you don't have to specify the value type if you don't need the value.

# squirrelbind 1.0 #
--------------------
 - Initial release.
