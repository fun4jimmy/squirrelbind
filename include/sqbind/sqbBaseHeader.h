#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <cstdint>
#include <cstddef>
//----------------------------------------------------------------------------------------------------------------------
#include <squirrel.h>
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
/// \def SQBIND_COMPILER_CLANG
/// \def SQBIND_COMPILER_MSVC
/// \def SQBIND_COMPILER_GCC
/// \def SQBIND_COMPILER_VERSION
//----------------------------------------------------------------------------------------------------------------------
#if defined(__clang__)
# define SQBIND_COMPILER_CLANG
# define SQBIND_COMPILER_VERSION (__clang_major__ * 10000) + (__clang_minor__ * 100) + __clang_patchlevel__
#elif defined(_MSC_VER)
# define SQBIND_COMPILER_MSVC
# define SQBIND_COMPILER_VERSION _MSC_VER
#elif defined(__GNUC__)
# define SQBIND_COMPILER_GCC
# define SQBIND_COMPILER_VERSION (__GNUC__ * 10000) + (__GNUC_MINOR__ * 100) + __GNUC_PATCHLEVEL__
#endif

//----------------------------------------------------------------------------------------------------------------------
/// \def SQBIND_MAJOR_VERSION
/// \brief The major version number of sqbind.
//----------------------------------------------------------------------------------------------------------------------
#define SQBIND_MAJOR_VERSION 2

//----------------------------------------------------------------------------------------------------------------------
/// \def SQBIND_MINOR_VERSION
/// \brief The minor version number of sqbind.
//----------------------------------------------------------------------------------------------------------------------
#define SQBIND_MINOR_VERSION 1

//----------------------------------------------------------------------------------------------------------------------
/// \def SQBIND_VERSION_NUMBER
/// \brief The combined version number of sqbind, of the form 1000.
/// The number will be (SQBIND_MAJOR_VERSION * 1000) + SQBIND_MINOR_VERSION
//----------------------------------------------------------------------------------------------------------------------
#define SQBIND_VERSION_NUMBER ((SQBIND_MAJOR_VERSION * 1000) + SQBIND_MINOR_VERSION)

//----------------------------------------------------------------------------------------------------------------------
/// \def SQBIND_STRINGIFY_INTERNAL
/// \brief Used by SQBIND_STRINGIFY to turn macro values in to strings, never call this directly.
//----------------------------------------------------------------------------------------------------------------------
#define SQBIND_STRINGIFY_INTERNAL(SQBIND_VALUE) _SC(#SQBIND_VALUE)

//----------------------------------------------------------------------------------------------------------------------
/// \def SQBIND_STRINGIFY
/// \brief Converts a value to a string.
/// \code
/// #define NUMBER 1.0
/// // str will be _SC("1.0")
/// //
/// const SQChar* str = SQBIND_STRINGIFY(NUMBER);
/// \endcode
//----------------------------------------------------------------------------------------------------------------------
#define SQBIND_STRINGIFY(SQBIND_VALUE) SQBIND_STRINGIFY_INTERNAL(SQBIND_VALUE)

//----------------------------------------------------------------------------------------------------------------------
/// \def SQBIND_VERSION_STRING
/// \brief The string version number of sqbind of the form sqbind-SQBIND_MAJOR_VERSION.SQBIND_MINOR_VERSION
//----------------------------------------------------------------------------------------------------------------------
#define SQBIND_VERSION_STRING _SC("sqbind-") SQBIND_STRINGIFY(SQBIND_MAJOR_VERSION) _SC(".") SQBIND_STRINGIFY(SQBIND_MINOR_VERSION)

//----------------------------------------------------------------------------------------------------------------------
/// \def SQBIND_UNUSED
/// \brief Used to mark a function parameter as unused
/// \code
/// void func(int SQBIND_UNUSED(blah))
/// {
/// }
/// \endcode
//----------------------------------------------------------------------------------------------------------------------
#define SQBIND_UNUSED(X)

//----------------------------------------------------------------------------------------------------------------------
/// \def SQBIND_API
/// \brief Used to mark objects for export when compiling sqbind as a dll.
//----------------------------------------------------------------------------------------------------------------------
#ifndef SQBIND_API
# ifdef WIN32
#  ifndef SQBIND_STATIC
#   ifdef SQBIND_EXPORTS
#    define SQBIND_API __declspec(dllexport)
#   else
#    define SQBIND_API __declspec(dllimport)
#   endif
#  else
#   define SQBIND_API
#  endif
# else
#  define SQBIND_API
# endif
#endif
