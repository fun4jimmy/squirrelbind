#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the 'Software'), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <sqbind/sqbStackHandler.h>
#include <sqbind/sqbStackUtils.h>
//----------------------------------------------------------------------------------------------------------------------

namespace sqb
{
//----------------------------------------------------------------------------------------------------------------------
// ReturnSpecialisation<ReturnType>
//----------------------------------------------------------------------------------------------------------------------
template<typename ReturnType>
class ReturnSpecialisation
{
public:
  /// \brief Type checks and retrieves the parameters from the stack and calls function.
  template<typename ... ParameterTypes>
  static SQRESULT Call(ReturnType (*function)(ParameterTypes ...), HSQUIRRELVM vm, SQInteger index);

  /// \brief Type checks and retrieves the parameters from the stack and calls the member function for instance.
  template<typename InstanceType, typename ... ParameterTypes>
  static SQRESULT Call(InstanceType &instance, ReturnType (InstanceType::*function)(ParameterTypes ...), HSQUIRRELVM vm, SQInteger index);

  /// \brief Type checks and retrieves the parameters from the stack and calls the const member function for instance.
  template<typename InstanceType, typename ... ParameterTypes>
  static SQRESULT Call(const InstanceType &instance, ReturnType (InstanceType::*function)(ParameterTypes ...) const, HSQUIRRELVM vm, SQInteger index);
};

} // namespace sqb

#include <sqbind/sqbReturnSpecialisation.inl>
