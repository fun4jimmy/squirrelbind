#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the 'Software'), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------

namespace sqb
{
namespace internal
{
//----------------------------------------------------------------------------------------------------------------------
// ParamCheckHelperImpl
//----------------------------------------------------------------------------------------------------------------------
template<typename ... ParameterTypes>
struct ParamCheckHelperImpl;

//----------------------------------------------------------------------------------------------------------------------
template<typename ParameterType, typename ... RemainingParameterTypes>
struct ParamCheckHelperImpl<ParameterType, RemainingParameterTypes ...>
{
  // recursive case writes one parameter type to typeMask and calls PopulateTypeMask for the remaining parameter types
  static inline void PopulateTypeMask(SQChar *typeMask, const size_t offset)
  {
    typeMask[offset] = sqb::TypeInfo<typename std::decay<ParameterType>::type>::kTypeMask;
    ParamCheckHelperImpl<RemainingParameterTypes ...>::PopulateTypeMask(typeMask, offset + 1);
  }
};

//----------------------------------------------------------------------------------------------------------------------
template<>
struct ParamCheckHelperImpl<>
{
  // terminating case ends the recursion
  static inline void PopulateTypeMask(SQChar *SQBIND_UNUSED(typeMask), const size_t SQBIND_UNUSED(offset))
  {
  }
};

} // namespace internal

//----------------------------------------------------------------------------------------------------------------------
// ParamCheckHelper
//----------------------------------------------------------------------------------------------------------------------
template<typename ReturnType, typename ... ParameterTypes>
inline SQRESULT ParamCheckHelper::SetParamsCheck(HSQUIRRELVM vm, ReturnType (*)(ParameterTypes ...))
{
  SQChar typeMask[sizeof...(ParameterTypes) + 2];
  typeMask[0] = _SC('.');
  internal::ParamCheckHelperImpl<ParameterTypes ...>::PopulateTypeMask(typeMask, 1);
  typeMask[sizeof...(ParameterTypes) + 1] = _SC('\0');

  return sq_setparamscheck(vm, sizeof ... (ParameterTypes) + 1, typeMask);
}

//----------------------------------------------------------------------------------------------------------------------
template<typename InstanceType, typename ReturnType, typename ... ParameterTypes>
inline SQRESULT ParamCheckHelper::SetParamsCheck(HSQUIRRELVM vm, ReturnType (InstanceType::*)(ParameterTypes ...))
{
  SQChar typeMask[sizeof...(ParameterTypes) + 2];
  typeMask[0] = _SC('.');
  internal::ParamCheckHelperImpl<ParameterTypes ...>::PopulateTypeMask(typeMask, 1);
  typeMask[sizeof...(ParameterTypes) + 1] = _SC('\0');

  return sq_setparamscheck(vm, sizeof ... (ParameterTypes) + 1, typeMask);
}

//----------------------------------------------------------------------------------------------------------------------
template<typename InstanceType, typename ReturnType, typename ... ParameterTypes>
inline SQRESULT ParamCheckHelper::SetParamsCheck(HSQUIRRELVM vm, ReturnType (InstanceType::*)(ParameterTypes ...) const)
{
  SQChar typeMask[sizeof...(ParameterTypes) + 2];
  typeMask[0] = _SC('.');
  internal::ParamCheckHelperImpl<ParameterTypes ...>::PopulateTypeMask(typeMask, 1);
  typeMask[sizeof...(ParameterTypes) + 1] = _SC('\0');

  return sq_setparamscheck(vm, sizeof ... (ParameterTypes) + 1, typeMask);
}

} // namespace sqb
