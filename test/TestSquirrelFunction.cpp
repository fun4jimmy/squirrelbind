//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <gmock/gmock.h>
//----------------------------------------------------------------------------------------------------------------------
#include <squirrel.h>
#include <sqstdstring.h>
//----------------------------------------------------------------------------------------------------------------------
#include <sqbind/sqbSquirrelFunction.h>
//----------------------------------------------------------------------------------------------------------------------
#include "fixtures/SquirrelFixture.h"
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
class SquirrelFunctionTest : public SquirrelFixture
{
public:
  virtual void SetUp() override;
};

//----------------------------------------------------------------------------------------------------------------------
void SquirrelFunctionTest::SetUp()
{
  SquirrelFixture::SetUp();
  sq_pushroottable(m_vm);
  ASSERT_SQ_SUCCEEDED(m_vm, sqstd_register_stringlib(m_vm));
  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(SquirrelFunctionTest, TestBasics)
{
  sq_pushroottable(m_vm);
  sq_pushstring(m_vm, _SC("format"), -1);
  ASSERT_SQ_SUCCEEDED(m_vm, sq_rawget(m_vm, -2));

  sqb::SquirrelFunction format(m_vm, -1);

  // pop the function and root table
  //
  sq_pop(m_vm, 2);

  format.AddArgument(_SC("this %s test"));
  format.AddArgument(_SC("is a"));

  EXPECT_SQ_SUCCEEDED(m_vm, format.Call());
  EXPECT_STREQ(_SC("this is a test"), format.GetReturnValue<const SQChar *>());

  format.ClearArguments();

  format.AddArgument(_SC("this %s test"));
  format.AddArgument(_SC("is another"));

  EXPECT_SQ_SUCCEEDED(m_vm, format.Call());
  EXPECT_STREQ(_SC("this is another test"), format.GetReturnValue<const SQChar *>());
}
