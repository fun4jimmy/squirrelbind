#include <stdarg.h>

#include <cstdlib>
#include <limits>
#include <string>

#include <squirrel.h>

#include <sqstdstring.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>
