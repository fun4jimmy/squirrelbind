#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "mocks/MockReleaseHook.h"
//----------------------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------------------
TEST_F(TEST_NAME, TestGetClassObject)
{
  sq_pushroottable(m_vm);

  BaseClassDefinitionType definition(m_vm, -1, _SC("test"));
  EXPECT_TRUE(sq_isclass(definition.GetClassObject()));

  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TEST_NAME, TestSetUserDataSize)
{
  sq_pushroottable(m_vm);

  BaseClassDefinitionType definition(m_vm, -1, _SC("test"));
  definition.SetUserDataSize(32);
  HSQOBJECT classObject = definition.GetClassObject();
  sq_pushobject(m_vm, classObject);
  EXPECT_EQ(32, sq_getsize(m_vm, -1));
  sq_poptop(m_vm);

  sq_poptop(m_vm);
}

#if HAS_CONSTRUCTOR

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TEST_NAME, TestSetReleaseHook)
{
  sq_pushroottable(m_vm);

  MockReleaseHook releaseHook;
  MockReleaseHook::m_instance = &releaseHook;

  BaseClassDefinitionType definition(m_vm, -1, _SC("Test"));
  definition.SetReleaseHook(&MockReleaseHook::ReleaseHook);

  EXPECT_CALL(releaseHook, ClassReleaseHook(testing::_, testing::_));

  CompileAndSucceedCall(_SC("local t = Test(); t = null"));

  MockReleaseHook::m_instance = nullptr;

  sq_poptop(m_vm);
}

#endif // #if HAS_CONSTRUCTOR

#if SUPPORTS_ALIGNMENT

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TEST_NAME, TestAlignment)
{
  NAMESPACE_NAME::AlignedClass* instance = CompileAndCallReturnResult<NAMESPACE_NAME::AlignedClass*>(_SC("return AlignedClass()"));
  ASSERT_TRUE(instance != nullptr);
  EXPECT_IS_ALIGNED(instance, 16);

  NAMESPACE_NAME::AlignedClass original;
  ASSERT_SQ_SUCCEEDED(m_vm, sqb::Push(m_vm, original));
  NAMESPACE_NAME::AlignedClass* copy = GetTypeFromStack<NAMESPACE_NAME::AlignedClass*>(-1);
  ASSERT_TRUE(copy != nullptr);
  EXPECT_IS_ALIGNED(copy, 16);

  sq_poptop(m_vm);
}

#endif // #if SUPPORTS_ALIGNMENT

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TEST_NAME, TestConstructor)
{
  // todo: implement
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TEST_NAME, TestClassFunctions)
{
  EXPECT_CALL(m_mockBaseClass, BaseClassFunction());
  CompileAndSucceedCall(_SC("m_mockBaseClass.BaseClassFunction()"));

  EXPECT_CALL(m_mockDerivedClassNoOffset, BaseClassFunction());
  CompileAndSucceedCall(_SC("m_mockDerivedClassNoOffset.BaseClassFunction()"));
  EXPECT_CALL(m_mockDerivedClassNoOffset, DerivedClassFunction());
  CompileAndSucceedCall(_SC("m_mockDerivedClassNoOffset.DerivedClassFunction()"));

  EXPECT_CALL(m_mockDerivedClassWithOffset, BaseClassFunction());
  CompileAndSucceedCall(_SC("m_mockDerivedClassWithOffset.BaseClassFunction()"));
  EXPECT_CALL(m_mockDerivedClassWithOffset, DerivedClassFunction());
  CompileAndSucceedCall(_SC("m_mockDerivedClassWithOffset.DerivedClassFunction()"));
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TEST_NAME, TestConstClassFunctions)
{
  EXPECT_CALL(m_mockBaseClass, BaseConstClassFunction());
  CompileAndSucceedCall(_SC("m_mockBaseClass.BaseConstClassFunction()"));

  EXPECT_CALL(m_mockDerivedClassNoOffset, BaseConstClassFunction());
  CompileAndSucceedCall(_SC("m_mockDerivedClassNoOffset.BaseConstClassFunction()"));
  EXPECT_CALL(m_mockDerivedClassNoOffset, DerivedConstClassFunction());
  CompileAndSucceedCall(_SC("m_mockDerivedClassNoOffset.DerivedConstClassFunction()"));

  EXPECT_CALL(m_mockDerivedClassWithOffset, BaseConstClassFunction());
  CompileAndSucceedCall(_SC("m_mockDerivedClassWithOffset.BaseConstClassFunction()"));
  EXPECT_CALL(m_mockDerivedClassWithOffset, DerivedConstClassFunction());
  CompileAndSucceedCall(_SC("m_mockDerivedClassWithOffset.DerivedConstClassFunction()"));
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TEST_NAME, TestNativeClassFunctions)
{
  EXPECT_CALL(m_mockBaseClass, BaseNativeClassFunction(m_vm))
    .WillOnce(testing::Return(0));
  CompileAndSucceedCall(_SC("m_mockBaseClass.BaseNativeClassFunction()"));

  EXPECT_CALL(m_mockDerivedClassNoOffset, BaseNativeClassFunction(m_vm))
    .WillOnce(testing::Return(0));
  CompileAndSucceedCall(_SC("m_mockDerivedClassNoOffset.BaseNativeClassFunction()"));
  EXPECT_CALL(m_mockDerivedClassNoOffset, DerivedNativeClassFunction(m_vm))
    .WillOnce(testing::Return(0));
  CompileAndSucceedCall(_SC("m_mockDerivedClassNoOffset.DerivedNativeClassFunction()"));

  EXPECT_CALL(m_mockDerivedClassWithOffset, BaseNativeClassFunction(m_vm))
    .WillOnce(testing::Return(0));
  CompileAndSucceedCall(_SC("m_mockDerivedClassWithOffset.BaseNativeClassFunction()"));
  EXPECT_CALL(m_mockDerivedClassWithOffset, DerivedNativeClassFunction(m_vm))
    .WillOnce(testing::Return(0));
  CompileAndSucceedCall(_SC("m_mockDerivedClassWithOffset.DerivedNativeClassFunction()"));
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TEST_NAME, TestFunctions)
{
  NAMESPACE_NAME::MockBaseClass::m_instance = &m_mockBaseClass;
  EXPECT_CALL(m_mockBaseClass, BaseClassFunction());
  CompileAndSucceedCall(_SC("m_mockBaseClass.BaseFunction()"));

  NAMESPACE_NAME::MockBaseClass::m_instance = &m_mockDerivedClassNoOffset;
  EXPECT_CALL(m_mockDerivedClassNoOffset, BaseClassFunction());
  CompileAndSucceedCall(_SC("m_mockDerivedClassNoOffset.BaseFunction()"));
  EXPECT_CALL(m_mockDerivedClassNoOffset, DerivedClassFunction());
  CompileAndSucceedCall(_SC("m_mockDerivedClassNoOffset.DerivedFunction()"));

  NAMESPACE_NAME::MockBaseClass::m_instance = &m_mockDerivedClassWithOffset;
  EXPECT_CALL(m_mockDerivedClassWithOffset, BaseClassFunction());
  CompileAndSucceedCall(_SC("m_mockDerivedClassWithOffset.BaseFunction()"));
  EXPECT_CALL(m_mockDerivedClassWithOffset, DerivedClassFunction());
  CompileAndSucceedCall(_SC("m_mockDerivedClassWithOffset.DerivedFunction()"));

  NAMESPACE_NAME::MockBaseClass::m_instance = nullptr;
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TEST_NAME, TestNativeFunctions)
{
  NAMESPACE_NAME::MockBaseClass::m_instance = &m_mockBaseClass;
  EXPECT_CALL(m_mockBaseClass, BaseNativeClassFunction(m_vm))
    .WillOnce(testing::Return(0));
  CompileAndSucceedCall(_SC("m_mockBaseClass.BaseNativeFunction()"));

  NAMESPACE_NAME::MockBaseClass::m_instance = &m_mockDerivedClassNoOffset;
  EXPECT_CALL(m_mockDerivedClassNoOffset, BaseNativeClassFunction(m_vm))
    .WillOnce(testing::Return(0));
  CompileAndSucceedCall(_SC("m_mockDerivedClassNoOffset.BaseNativeFunction()"));
  EXPECT_CALL(m_mockDerivedClassNoOffset, DerivedNativeClassFunction(m_vm))
    .WillOnce(testing::Return(0));
  CompileAndSucceedCall(_SC("m_mockDerivedClassNoOffset.DerivedNativeFunction()"));

  NAMESPACE_NAME::MockBaseClass::m_instance = &m_mockDerivedClassWithOffset;
  EXPECT_CALL(m_mockDerivedClassWithOffset, BaseNativeClassFunction(m_vm))
    .WillOnce(testing::Return(0));
  CompileAndSucceedCall(_SC("m_mockDerivedClassWithOffset.BaseNativeFunction()"));
  EXPECT_CALL(m_mockDerivedClassWithOffset, DerivedNativeClassFunction(m_vm))
    .WillOnce(testing::Return(0));
  CompileAndSucceedCall(_SC("m_mockDerivedClassWithOffset.DerivedNativeFunction()"));

  NAMESPACE_NAME::MockBaseClass::m_instance = nullptr;
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TEST_NAME, TestSingletonFunctions)
{
  EXPECT_CALL(m_mockBaseClass, BaseClassFunction());
  CompileAndSucceedCall(_SC("MockBaseClass.BaseSingletonFunction()"));

  EXPECT_CALL(m_mockBaseClass, BaseClassFunction());
  CompileAndSucceedCall(_SC("MockDerivedClassNoOffset.BaseSingletonFunction()"));
  EXPECT_CALL(m_mockDerivedClassNoOffset, DerivedClassFunction());
  CompileAndSucceedCall(_SC("MockDerivedClassNoOffset.DerivedSingletonFunction()"));

  EXPECT_CALL(m_mockBaseClass, BaseClassFunction());
  CompileAndSucceedCall(_SC("MockDerivedClassWithOffset.BaseSingletonFunction()"));
  EXPECT_CALL(m_mockDerivedClassWithOffset, DerivedClassFunction());
  CompileAndSucceedCall(_SC("MockDerivedClassWithOffset.DerivedSingletonFunction()"));
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TEST_NAME, TestConstSingletonFunctions)
{
  EXPECT_CALL(m_mockBaseClass, BaseConstClassFunction());
  CompileAndSucceedCall(_SC("MockBaseClass.BaseConstSingletonFunction()"));

  EXPECT_CALL(m_mockBaseClass, BaseConstClassFunction());
  CompileAndSucceedCall(_SC("MockDerivedClassNoOffset.BaseConstSingletonFunction()"));
  EXPECT_CALL(m_mockDerivedClassNoOffset, DerivedConstClassFunction());
  CompileAndSucceedCall(_SC("MockDerivedClassNoOffset.DerivedConstSingletonFunction()"));

  EXPECT_CALL(m_mockBaseClass, BaseConstClassFunction());
  CompileAndSucceedCall(_SC("MockDerivedClassWithOffset.BaseConstSingletonFunction()"));
  EXPECT_CALL(m_mockDerivedClassWithOffset, DerivedConstClassFunction());
  CompileAndSucceedCall(_SC("MockDerivedClassWithOffset.DerivedConstSingletonFunction()"));
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TEST_NAME, TestNativeSingletonFunctions)
{
  EXPECT_CALL(m_mockBaseClass, BaseNativeClassFunction(m_vm))
    .Times(3)
    .WillRepeatedly(testing::Return(0));
  CompileAndSucceedCall(_SC("MockBaseClass.BaseNativeSingletonFunction()"));

  CompileAndSucceedCall(_SC("MockDerivedClassNoOffset.BaseNativeSingletonFunction()"));
  EXPECT_CALL(m_mockDerivedClassNoOffset, DerivedNativeClassFunction(m_vm))
    .WillOnce(testing::Return(0));
  CompileAndSucceedCall(_SC("MockDerivedClassNoOffset.DerivedNativeSingletonFunction()"));

  CompileAndSucceedCall(_SC("MockDerivedClassWithOffset.BaseNativeSingletonFunction()"));
  EXPECT_CALL(m_mockDerivedClassWithOffset, DerivedNativeClassFunction(m_vm))
    .WillOnce(testing::Return(0));
  CompileAndSucceedCall(_SC("MockDerivedClassWithOffset.DerivedNativeSingletonFunction()"));
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TEST_NAME, TestVariable)
{
  m_mockBaseClass.m_baseInstanceValue = kExpectedInteger;
  EXPECT_EQ(kExpectedInteger, CompileAndCallReturnResult<int32_t>(_SC("return m_mockBaseClass.m_baseInstanceValue")));
  CompileAndSucceedCall(_SC("m_mockBaseClass.m_baseInstanceValue = 0"));
  EXPECT_EQ(0, m_mockBaseClass.m_baseInstanceValue);

  m_mockDerivedClassNoOffset.m_baseInstanceValue = kExpectedInteger;
  EXPECT_EQ(kExpectedInteger, CompileAndCallReturnResult<int32_t>(_SC("return m_mockDerivedClassNoOffset.m_baseInstanceValue")));
  CompileAndSucceedCall(_SC("m_mockDerivedClassNoOffset.m_baseInstanceValue = 1"));
  EXPECT_EQ(1, m_mockDerivedClassNoOffset.m_baseInstanceValue);
  m_mockDerivedClassNoOffset.m_derivedInstanceValue = kExpectedInteger;
  EXPECT_EQ(kExpectedInteger, CompileAndCallReturnResult<int32_t>(_SC("return m_mockDerivedClassNoOffset.m_derivedInstanceValue")));
  CompileAndSucceedCall(_SC("m_mockDerivedClassNoOffset.m_derivedInstanceValue = 2"));
  EXPECT_EQ(2, m_mockDerivedClassNoOffset.m_derivedInstanceValue);

  m_mockDerivedClassWithOffset.m_baseInstanceValue = kExpectedInteger;
  EXPECT_EQ(kExpectedInteger, CompileAndCallReturnResult<int32_t>(_SC("return m_mockDerivedClassWithOffset.m_baseInstanceValue")));
  CompileAndSucceedCall(_SC("m_mockDerivedClassWithOffset.m_baseInstanceValue = 1"));
  EXPECT_EQ(1, m_mockDerivedClassWithOffset.m_baseInstanceValue);
  m_mockDerivedClassWithOffset.m_derivedInstanceValue = kExpectedInteger;
  EXPECT_EQ(kExpectedInteger, CompileAndCallReturnResult<int32_t>(_SC("return m_mockDerivedClassWithOffset.m_derivedInstanceValue")));
  CompileAndSucceedCall(_SC("m_mockDerivedClassWithOffset.m_derivedInstanceValue = 2"));
  EXPECT_EQ(2, m_mockDerivedClassWithOffset.m_derivedInstanceValue);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TEST_NAME, TestStaticVariable)
{
  EXPECT_EQ(
    NAMESPACE_NAME::MockBaseClass::m_baseStaticValue,
    CompileAndCallReturnResult<int32_t>(_SC("return MockBaseClass.m_baseStaticValue")));

  EXPECT_EQ(
    NAMESPACE_NAME::MockDerivedClassNoOffset::m_baseStaticValue,
    CompileAndCallReturnResult<int32_t>(_SC("return MockDerivedClassNoOffset.m_baseStaticValue")));
  EXPECT_EQ(
    NAMESPACE_NAME::MockDerivedClassNoOffset::m_derivedStaticValue,
    CompileAndCallReturnResult<int32_t>(_SC("return MockDerivedClassNoOffset.m_derivedStaticValue")));

  EXPECT_EQ(
    NAMESPACE_NAME::MockDerivedClassWithOffset::m_baseStaticValue,
    CompileAndCallReturnResult<int32_t>(_SC("return MockDerivedClassWithOffset.m_baseStaticValue")));
  EXPECT_EQ(
    NAMESPACE_NAME::MockDerivedClassWithOffset::m_derivedStaticValue,
    CompileAndCallReturnResult<int32_t>(_SC("return MockDerivedClassWithOffset.m_derivedStaticValue")));
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TEST_NAME, TestEnumEntry)
{
  EXPECT_EQ(
    NAMESPACE_NAME::MockBaseClass::kBaseEnumValue,
    CompileAndCallReturnResult<int32_t>(_SC("return MockBaseClass.kBaseEnumValue")));

  EXPECT_EQ(
    NAMESPACE_NAME::MockDerivedClassNoOffset::kBaseEnumValue,
    CompileAndCallReturnResult<int32_t>(_SC("return MockDerivedClassNoOffset.kBaseEnumValue")));
  EXPECT_EQ(
    NAMESPACE_NAME::MockDerivedClassNoOffset::kDerivedEnumValue,
    CompileAndCallReturnResult<int32_t>(_SC("return MockDerivedClassNoOffset.kDerivedEnumValue")));

  EXPECT_EQ(
    NAMESPACE_NAME::MockDerivedClassWithOffset::kBaseEnumValue,
    CompileAndCallReturnResult<int32_t>(_SC("return MockDerivedClassWithOffset.kBaseEnumValue")));
  EXPECT_EQ(
    NAMESPACE_NAME::MockDerivedClassWithOffset::kDerivedEnumValue,
    CompileAndCallReturnResult<int32_t>(_SC("return MockDerivedClassWithOffset.kDerivedEnumValue")));
}
