//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <string>

#include <gmock/gmock.h>

#include <sqbind/sqbBaseHeader.h>
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
// to_string_impl
//----------------------------------------------------------------------------------------------------------------------
template<typename TChar>
std::basic_string<TChar> to_string_impl(uint32_t number);

template<>
std::basic_string<char> to_string_impl(uint32_t number)
{
  return std::to_string(number);
}

template<>
std::basic_string<wchar_t> to_string_impl(uint32_t number)
{
  return std::to_wstring(number);
}

//----------------------------------------------------------------------------------------------------------------------
// to_string
//----------------------------------------------------------------------------------------------------------------------
std::basic_string<SQChar> to_string(uint32_t number)
{
  return to_string_impl<SQChar>(number);
}

//----------------------------------------------------------------------------------------------------------------------
TEST(VersionTest, TestVersionNumber)
{
  EXPECT_EQ(SQBIND_MAJOR_VERSION * 1000 + SQBIND_MINOR_VERSION, SQBIND_VERSION_NUMBER);
}

//----------------------------------------------------------------------------------------------------------------------
TEST(VersionTest, TestVersionString)
{
  std::basic_string<SQChar> versionString = _SC("sqbind-");
  versionString += to_string(SQBIND_MAJOR_VERSION);
  versionString += _SC(".");
  versionString += to_string(SQBIND_MINOR_VERSION);
  EXPECT_STREQ(versionString.c_str(), SQBIND_VERSION_STRING);
}
