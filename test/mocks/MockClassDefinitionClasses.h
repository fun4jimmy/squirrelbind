#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <gmock/gmock.h>
//----------------------------------------------------------------------------------------------------------------------
#include <squirrel.h>
//----------------------------------------------------------------------------------------------------------------------
#include "../fixtures/SquirrelFixture.h"
//----------------------------------------------------------------------------------------------------------------------


#if !defined(NAMESPACE_NAME)
#error
#endif

namespace NAMESPACE_NAME
{
//----------------------------------------------------------------------------------------------------------------------
class MockBaseClass
{
public:
  static MockBaseClass *m_instance;

  enum
  {
    kBaseEnumValue,
  };

  static SQInteger  m_baseStaticValue;
  SQInteger         m_baseInstanceValue;

  MockBaseClass();
  ~MockBaseClass();

  MOCK_METHOD0(BaseClassFunction, void());
  MOCK_CONST_METHOD0(BaseConstClassFunction, void());

  MOCK_METHOD1(BaseNativeClassFunction, SQInteger(HSQUIRRELVM));

  static void BaseFunction();
  static SQInteger BaseNativeFunction(HSQUIRRELVM vm);
};

//----------------------------------------------------------------------------------------------------------------------
class MockDerivedClassNoOffset : public MockBaseClass
{
public:

  enum
  {
    kDerivedEnumValue,
  };

  static SQInteger  m_derivedStaticValue;
  SQInteger         m_derivedInstanceValue;

  MockDerivedClassNoOffset();
  ~MockDerivedClassNoOffset();

  MOCK_METHOD0(DerivedClassFunction, void());
  MOCK_CONST_METHOD0(DerivedConstClassFunction, void());

  MOCK_METHOD1(DerivedNativeClassFunction, SQInteger(HSQUIRRELVM));

  static void DerivedFunction();
  static SQInteger DerivedNativeFunction(HSQUIRRELVM vm);
};

//----------------------------------------------------------------------------------------------------------------------
class MockDerivedClassWithOffset : public MockBaseClass
{
public:

  enum
  {
    kDerivedEnumValue,
  };

  static SQInteger  m_derivedStaticValue;
  SQInteger         m_derivedInstanceValue;

  MockDerivedClassWithOffset();
  virtual ~MockDerivedClassWithOffset();

  MOCK_METHOD0(DerivedClassFunction, void());
  MOCK_CONST_METHOD0(DerivedConstClassFunction, void());

  MOCK_METHOD1(DerivedNativeClassFunction, SQInteger(HSQUIRRELVM));

  static void DerivedFunction();
  static SQInteger DerivedNativeFunction(HSQUIRRELVM vm);
};

#if defined(SQBIND_COMPILER_MSVC)

#define PRE_ALIGN(ALIGNMENT) __declspec(align(ALIGNMENT))
#define POST_ALIGN(ALIGNMENT)

#else

#define PRE_ALIGN(ALIGNMENT)
#define POST_ALIGN(ALIGNMENT) __attribute__ ((aligned(16)))

#endif

//----------------------------------------------------------------------------------------------------------------------
PRE_ALIGN(16) struct AlignedClass { uint8_t m_padding[16]; } POST_ALIGN(16);

} // namespace NAMESPACE_NAME

SQBIND_TYPE_NON_COPY_CONSTRUCTIBLE(NAMESPACE_NAME::MockBaseClass);
SQBIND_TYPE_NON_COPY_ASSIGNABLE(NAMESPACE_NAME::MockBaseClass);
SQBIND_TYPE_NON_COPY_CONSTRUCTIBLE(NAMESPACE_NAME::MockDerivedClassNoOffset);
SQBIND_TYPE_NON_COPY_ASSIGNABLE(NAMESPACE_NAME::MockDerivedClassNoOffset);
SQBIND_TYPE_NON_COPY_CONSTRUCTIBLE(NAMESPACE_NAME::MockDerivedClassWithOffset);
SQBIND_TYPE_NON_COPY_ASSIGNABLE(NAMESPACE_NAME::MockDerivedClassWithOffset);

#include "mocks/MockClassDefinitionClasses.inl"
