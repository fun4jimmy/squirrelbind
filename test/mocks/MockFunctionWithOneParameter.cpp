//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the 'Software'), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "MockFunctionWithOneParameter.h"
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
// MockFunctionWithOneParameter
//----------------------------------------------------------------------------------------------------------------------
MockFunctionWithOneParameter *MockFunctionWithOneParameter::m_instance = nullptr;

//----------------------------------------------------------------------------------------------------------------------
MockFunctionWithOneParameter::MockFunctionWithOneParameter()
{
}

//----------------------------------------------------------------------------------------------------------------------
MockFunctionWithOneParameter::~MockFunctionWithOneParameter()
{
}

//----------------------------------------------------------------------------------------------------------------------
void MockFunctionWithOneParameter::VoidFunction(BoundClass parameter0)
{
  return m_instance->ClassVoidFunction(parameter0);
}

//----------------------------------------------------------------------------------------------------------------------
BoundClass MockFunctionWithOneParameter::Function(BoundClass parameter0)
{
  return m_instance->ClassFunction(parameter0);
}

//----------------------------------------------------------------------------------------------------------------------
InvalidStackUtilsClass MockFunctionWithOneParameter::InvalidFunction(BoundClass parameter0)
{
  return m_instance->ClassInvalidFunction(parameter0);
}
