//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "MockAssertionFailureHandler.h"
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
FailureHandler::FailureHandler(sqb::AssertFailOp failOperation)
: m_failOperation(failOperation)
{
}

//----------------------------------------------------------------------------------------------------------------------
FailureHandler::~FailureHandler()
{
}

//----------------------------------------------------------------------------------------------------------------------
sqb::AssertFailOp FailureHandler::FailureFunction(
  const char* SQBIND_UNUSED(file),
  size_t SQBIND_UNUSED(line),
  const char* SQBIND_UNUSED(message))
{
  return m_failOperation;
}

//----------------------------------------------------------------------------------------------------------------------
// MockFailureHandler
//----------------------------------------------------------------------------------------------------------------------
MockFailureHandler::MockFailureHandler()
{
}

//----------------------------------------------------------------------------------------------------------------------
MockFailureHandler::~MockFailureHandler()
{
}

//----------------------------------------------------------------------------------------------------------------------
// MockAssertionFailureFunction
//----------------------------------------------------------------------------------------------------------------------
sqb::AssertFailOp MockAssertionFailureFunction(
  const char *file,
  size_t line,
  const char *message,
  void *userData)
{
  FailureHandler *handler = static_cast<FailureHandler *>(userData);
  return handler->FailureFunction(file, line, message);
}

