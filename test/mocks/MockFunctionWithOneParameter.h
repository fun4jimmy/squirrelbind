#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the 'Software'), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <gmock/gmock.h>

#include "../TypeHelpers.h"
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
// MockFunctionWithOneParameter
//----------------------------------------------------------------------------------------------------------------------
class MockFunctionWithOneParameter
{
public:
  static MockFunctionWithOneParameter *m_instance;

  MockFunctionWithOneParameter();
  ~MockFunctionWithOneParameter();

  MOCK_METHOD1(ClassVoidFunction, void(BoundClass));
  MOCK_METHOD1(ClassFunction, BoundClass(BoundClass));
  MOCK_METHOD1(ClassInvalidFunction, InvalidStackUtilsClass(BoundClass));

  MOCK_CONST_METHOD1(ConstClassVoidFunction, void(BoundClass));
  MOCK_CONST_METHOD1(ConstClassFunction, BoundClass(BoundClass));
  MOCK_CONST_METHOD1(ConstClassInvalidFunction, InvalidStackUtilsClass(BoundClass));

  static void VoidFunction(BoundClass parameter0);
  static BoundClass Function(BoundClass parameter0);
  static InvalidStackUtilsClass InvalidFunction(BoundClass parameter0);
};

//----------------------------------------------------------------------------------------------------------------------
SQBIND_TYPE_NON_COPY_ASSIGNABLE(::MockFunctionWithOneParameter);

//----------------------------------------------------------------------------------------------------------------------
SQBIND_DECLARE_TYPEINFO(::MockFunctionWithOneParameter, MockFunctionWithOneParameter);
