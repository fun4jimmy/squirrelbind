#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the 'Software'), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <gmock/gmock.h>

#include "../TypeHelpers.h"
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
// MockFunctionWithNoParameters
//----------------------------------------------------------------------------------------------------------------------
class MockFunctionWithNoParameters
{
public:
  static MockFunctionWithNoParameters *m_instance;

  MockFunctionWithNoParameters();
  ~MockFunctionWithNoParameters();

  MOCK_METHOD0(ClassVoidFunction, void());
  MOCK_METHOD0(ClassFunction, BoundClass());
  MOCK_METHOD0(ClassInvalidFunction, InvalidStackUtilsClass());

  MOCK_CONST_METHOD0(ConstClassVoidFunction, void());
  MOCK_CONST_METHOD0(ConstClassFunction, BoundClass());
  MOCK_CONST_METHOD0(ConstClassInvalidFunction, InvalidStackUtilsClass());

  static void VoidFunction();
  static BoundClass Function();
  static InvalidStackUtilsClass InvalidFunction();
};

//----------------------------------------------------------------------------------------------------------------------
SQBIND_TYPE_NON_COPY_ASSIGNABLE(::MockFunctionWithNoParameters);

//----------------------------------------------------------------------------------------------------------------------
SQBIND_DECLARE_TYPEINFO(::MockFunctionWithNoParameters, MockFunctionWithNoParameters);
