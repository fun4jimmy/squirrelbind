#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------

namespace NAMESPACE_NAME
{
//----------------------------------------------------------------------------------------------------------------------
// MockBaseClass
//----------------------------------------------------------------------------------------------------------------------
MockBaseClass *MockBaseClass::m_instance = nullptr;
SQInteger MockBaseClass::m_baseStaticValue = 55;

//----------------------------------------------------------------------------------------------------------------------
MockBaseClass::MockBaseClass()
: m_baseInstanceValue(0)
{
}

//----------------------------------------------------------------------------------------------------------------------
MockBaseClass::~MockBaseClass()
{
}

//----------------------------------------------------------------------------------------------------------------------
void MockBaseClass::BaseFunction()
{
  m_instance->BaseClassFunction();
}

//----------------------------------------------------------------------------------------------------------------------
SQInteger MockBaseClass::BaseNativeFunction(HSQUIRRELVM vm)
{
  return m_instance->BaseNativeClassFunction(vm);
}

//----------------------------------------------------------------------------------------------------------------------
// MockDerivedClassNoOffset
//----------------------------------------------------------------------------------------------------------------------
SQInteger MockDerivedClassNoOffset::m_derivedStaticValue = 89;

//----------------------------------------------------------------------------------------------------------------------
MockDerivedClassNoOffset::MockDerivedClassNoOffset()
: MockBaseClass(),
  m_derivedInstanceValue(0)
{
}

//----------------------------------------------------------------------------------------------------------------------
MockDerivedClassNoOffset::~MockDerivedClassNoOffset()
{
}

//----------------------------------------------------------------------------------------------------------------------
void MockDerivedClassNoOffset::DerivedFunction()
{
  static_cast<MockDerivedClassNoOffset *>(m_instance)->DerivedClassFunction();
}

//----------------------------------------------------------------------------------------------------------------------
SQInteger MockDerivedClassNoOffset::DerivedNativeFunction(HSQUIRRELVM vm)
{
  return static_cast<MockDerivedClassNoOffset *>(m_instance)->DerivedNativeClassFunction(vm);
}

//----------------------------------------------------------------------------------------------------------------------
// MockDerivedClassWithOffset
//----------------------------------------------------------------------------------------------------------------------
SQInteger MockDerivedClassWithOffset::m_derivedStaticValue = 234;

//----------------------------------------------------------------------------------------------------------------------
MockDerivedClassWithOffset::MockDerivedClassWithOffset()
: m_derivedInstanceValue(0)
{
}

//----------------------------------------------------------------------------------------------------------------------
MockDerivedClassWithOffset::~MockDerivedClassWithOffset()
{
}

//----------------------------------------------------------------------------------------------------------------------
void MockDerivedClassWithOffset::DerivedFunction()
{
  static_cast<MockDerivedClassWithOffset *>(m_instance)->DerivedClassFunction();
}

//----------------------------------------------------------------------------------------------------------------------
SQInteger MockDerivedClassWithOffset::DerivedNativeFunction(HSQUIRRELVM vm)
{
  return static_cast<MockDerivedClassWithOffset *>(m_instance)->DerivedNativeClassFunction(vm);
}

} // namespace NAMESPACE_NAME

//----------------------------------------------------------------------------------------------------------------------
// AlignedClass
//----------------------------------------------------------------------------------------------------------------------
template<>
inline NAMESPACE_NAME::AlignedClass* SquirrelFixture::GetTypeFromStack<NAMESPACE_NAME::AlignedClass*>(SQInteger index)
{
  sqb::ClassTypeTagBase* classTypeTag = sqb::ClassTypeTag<NAMESPACE_NAME::AlignedClass>::Get();
  SQUserPointer value = nullptr;
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getinstanceup(m_vm, index, &value, classTypeTag));
  return static_cast<NAMESPACE_NAME::AlignedClass*>(value);
}
