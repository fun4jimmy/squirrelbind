//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "StringHelpers.h"
//----------------------------------------------------------------------------------------------------------------------
#include <cstdlib>
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
const SQChar* kRandomStrings[] = {
  _SC("1911"),
  _SC("1915"),
  _SC("1920."),
  _SC("1930s"),
  _SC("1931"),
  _SC("1959."),
  _SC("a"),
  _SC("aboard"),
  _SC("action"),
  _SC("after"),
  _SC("almirante"),
  _SC("american"),
  _SC("and"),
  _SC("apart"),
  _SC("approaching"),
  _SC("as"),
  _SC("attack"),
  _SC("back"),
  _SC("battle"),
  _SC("battleship"),
  _SC("began"),
  _SC("beginning"),
  _SC("between"),
  _SC("bought"),
  _SC("built"),
  _SC("but"),
  _SC("by"),
  _SC("canada"),
  _SC("chile"),
  _SC("chile's"),
  _SC("chile."),
  _SC("chilean"),
  _SC("class"),
  _SC("commissioned"),
  _SC("completion"),
  _SC("condition"),
  _SC("construction"),
  _SC("control."),
  _SC("countries."),
  _SC("crewmen"),
  _SC("declined"),
  _SC("depression"),
  _SC("developed"),
  _SC("divisions"),
  _SC("due"),
  _SC("duration"),
  _SC("during"),
  _SC("earlier"),
  _SC("economic"),
  _SC("enough"),
  _SC("fell"),
  _SC("first"),
  _SC("flagship"),
  _SC("fleet"),
  _SC("for"),
  _SC("frequently"),
  _SC("from"),
  _SC("good"),
  _SC("government"),
  _SC("grand"),
  _SC("harbor."),
  _SC("her"),
  _SC("hms"),
  _SC("in"),
  _SC("instigated"),
  _SC("interest"),
  _SC("into"),
  _SC("japan"),
  _SC("joined."),
  _SC("jutland."),
  _SC("kingdom's"),
  _SC("latorre"),
  _SC("majority"),
  _SC("most"),
  _SC("mutineers"),
  _SC("mutiny"),
  _SC("name"),
  _SC("navy"),
  _SC("navy."),
  _SC("november"),
  _SC("of"),
  _SC("on"),
  _SC("ordered"),
  _SC("original"),
  _SC("other"),
  _SC("patrol"),
  _SC("pearl"),
  _SC("planned"),
  _SC("presidential"),
  _SC("purchases"),
  _SC("put"),
  _SC("quickly"),
  _SC("rebellion"),
  _SC("receive"),
  _SC("repurchased"),
  _SC("reserve"),
  _SC("respond"),
  _SC("returned"),
  _SC("royal"),
  _SC("saw"),
  _SC("scrapped"),
  _SC("second"),
  _SC("september"),
  _SC("served"),
  _SC("severe"),
  _SC("she"),
  _SC("ship"),
  _SC("ships"),
  _SC("soon"),
  _SC("south"),
  _SC("spent"),
  _SC("states"),
  _SC("super-dreadnought"),
  _SC("that"),
  _SC("the"),
  _SC("this"),
  _SC("time"),
  _SC("to"),
  _SC("took"),
  _SC("transport."),
  _SC("two-ship"),
  _SC("united"),
  _SC("use"),
  _SC("war"),
  _SC("war."),
  _SC("warship"),
  _SC("was"),
  _SC("were"),
  _SC("when"),
  _SC("which"),
  _SC("world"),
  _SC("would"),
};

//----------------------------------------------------------------------------------------------------------------------
const size_t kRandomStringCount = sizeof(kRandomStrings) / sizeof(const SQChar *);

//----------------------------------------------------------------------------------------------------------------------
const SQChar *GetRandomString()
{
  uint32_t randomNumber = static_cast<uint32_t>(rand());
  uint32_t randomStringIndex = randomNumber % kRandomStringCount;
  return kRandomStrings[randomStringIndex];
}
