//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <gmock/gmock.h>
//----------------------------------------------------------------------------------------------------------------------
#include <sqbind/sqbScopedTableAccess.h>
#include <sqbind/sqbStackUtils.h>
//----------------------------------------------------------------------------------------------------------------------
#include "fixtures/TypeFixture.h"
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
template<typename TypeParam>
class ScopedTableAccessTypedValueTest : public BaseTypeFixture<TypeParam> {};
TYPED_TEST_CASE(ScopedTableAccessTypedValueTest, AllTypes);

//----------------------------------------------------------------------------------------------------------------------
template<typename TypeParam>
class ScopedTableAccessTypedKeyTest : public BaseTypeFixture<TypeParam> {};
TYPED_TEST_CASE(ScopedTableAccessTypedKeyTest, NativeTypes);

//----------------------------------------------------------------------------------------------------------------------
TYPED_TEST(ScopedTableAccessTypedValueTest, TestRawSetGetSlot)
{
  sq_pushroottable(this->m_vm);
  sqb::SquirrelObject roottable(this->m_vm, -1);
  sq_poptop(this->m_vm);

  const TypeParam expected = BaseTypeFixture<TypeParam>::GetRandomValue();
  TypeParam actual = BaseTypeFixture<TypeParam>::GetRandomValue();

  {
    sqb::ScopedTableAccess access(roottable);
    access.RawSetSlot(_SC("value"), expected);
    EXPECT_SQ_SUCCEEDED(this->m_vm, access.RawGetSlot(_SC("value"), &actual));
    EXPECT_TYPE_PARAM_EQ(expected, actual);
  }

  EXPECT_TYPE_PARAM_EQ(expected, SquirrelFixture::CompileAndCallReturnResult<TypeParam>(_SC("return value")));
}

//----------------------------------------------------------------------------------------------------------------------
TYPED_TEST(ScopedTableAccessTypedKeyTest, TestRawSetGetSlot)
{
  sq_pushroottable(this->m_vm);
  sqb::SquirrelObject roottable(this->m_vm, -1);
  sq_poptop(this->m_vm);

  const TypeParam key = BaseTypeFixture<TypeParam>::GetRandomValue();
  const SQChar *expected = _SC("value");
  const SQChar *actual = _SC("other");

  {
    sqb::ScopedTableAccess access(roottable);
    access.RawSetSlot(key, expected);
    EXPECT_SQ_SUCCEEDED(this->m_vm, access.RawGetSlot(key, &actual));
    EXPECT_STREQ(expected, actual);
  }
}

//----------------------------------------------------------------------------------------------------------------------
TYPED_TEST(ScopedTableAccessTypedKeyTest, TestRawSlotExists)
{
  sq_pushroottable(this->m_vm);
  sqb::SquirrelObject roottable(this->m_vm, -1);
  sq_poptop(this->m_vm);

  const TypeParam key = BaseTypeFixture<TypeParam>::GetRandomValue();
  const SQChar *expected = _SC("value");

  {
    sqb::ScopedTableAccess access(roottable);

    EXPECT_FALSE(access.RawSlotExists(key));
    EXPECT_SQ_SUCCEEDED(this->m_vm, access.RawSetSlot(key, expected));
    EXPECT_TRUE(access.RawSlotExists(key));
  }
}

//----------------------------------------------------------------------------------------------------------------------
TYPED_TEST(ScopedTableAccessTypedKeyTest, TestSlotExists)
{
  sq_pushroottable(this->m_vm);
  sqb::SquirrelObject roottable(this->m_vm, -1);
  sq_poptop(this->m_vm);

  const TypeParam key = BaseTypeFixture<TypeParam>::GetRandomValue();
  const SQChar *expected = _SC("value");

  {
    sqb::ScopedTableAccess access(roottable);

    EXPECT_FALSE(access.SlotExists(key));
    EXPECT_SQ_SUCCEEDED(this->m_vm, access.NewSlot(key, expected));
    EXPECT_TRUE(access.SlotExists(key));
  }
}
