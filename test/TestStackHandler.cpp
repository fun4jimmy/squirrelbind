//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <gmock/gmock.h>
//----------------------------------------------------------------------------------------------------------------------
#include <sqbind/sqbBindMacros.h>
#include <sqbind/sqbStackHandler.h>
//----------------------------------------------------------------------------------------------------------------------
#include "fixtures/SquirrelFixture.h"
#include "mocks/MockReleaseHook.h"
#include "TypeHelpers.h"
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
/// stack handler bound classes for testing
//----------------------------------------------------------------------------------------------------------------------
namespace
{
struct Klass
{
  Klass() {};
};

struct OtherKlass
{
  OtherKlass() {};
};

}

SQBIND_DECLARE_CUSTOM_CLASS(Klass);
SQBIND_DECLARE_CUSTOM_CLASS(OtherKlass);

namespace sqb
{
// add custom push functions for Klass so it will succeed.
//
template<>
SQRESULT Push<Klass>(HSQUIRRELVM vm, Klass *value)
{
  sq_pushuserpointer(vm, const_cast<Klass*>(value));
  return 1;
}
template<>
SQRESULT Push<Klass>(HSQUIRRELVM vm, Klass &value)
{
  sq_pushuserpointer(vm, const_cast<Klass*>(&value));
  return 1;
}
template<>
SQRESULT Push<Klass>(HSQUIRRELVM vm, const Klass *value)
{
  sq_pushuserpointer(vm, const_cast<Klass*>(value));
  return 1;
}
template<>
SQRESULT Push<Klass>(HSQUIRRELVM vm, const Klass &value)
{
  sq_pushuserpointer(vm, const_cast<Klass*>(&value));
  return 1;
}

// add custom push functions for OtherKlass so it will fail.
//
template<>
SQRESULT Push<OtherKlass>(HSQUIRRELVM vm, OtherKlass *SQBIND_UNUSED(value))
{
  return sq_throwerror(vm, _SC("error returning instance of type 'OtherKlass'"));
}
template<>
SQRESULT Push<OtherKlass>(HSQUIRRELVM vm, OtherKlass &SQBIND_UNUSED(value))
{
  return sq_throwerror(vm, _SC("error returning instance of type 'OtherKlass'"));
}
template<>
SQRESULT Push<OtherKlass>(HSQUIRRELVM vm, const OtherKlass *SQBIND_UNUSED(value))
{
  return sq_throwerror(vm, _SC("error returning instance of type 'OtherKlass'"));
}
template<>
SQRESULT Push<OtherKlass>(HSQUIRRELVM vm, const OtherKlass &SQBIND_UNUSED(value))
{
  return sq_throwerror(vm, _SC("error returning instance of type 'OtherKlass'"));
}
}

typedef SquirrelFixture StackHandlerTest;
typedef StackHandlerTest StackHandlerDeathTest;

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestConstructor)
{
  sqb::StackHandler sh(m_vm);
  EXPECT_EQ(m_vm, sh.GetVMPtr());
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestGetParamCount)
{
  sqb::StackHandler sh(m_vm);
  EXPECT_EQ(0, sh.GetParamCount());

  sq_pushnull(m_vm);

  EXPECT_EQ(1, sh.GetParamCount());

  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerDeathTest, TestNull)
{
  sqb::StackHandler sh(m_vm);

  // check null type
  //
  ASSERT_NO_FATAL_FAILURE(PushObjectOfType(m_vm, OT_NULL));
  EXPECT_EQ(OT_NULL, sh.GetType(-1));
  EXPECT_FALSE(sh.IsNumber(-1));
  EXPECT_FALSE(sh.IsString(-1));
  EXPECT_FALSE(sh.IsInstance(-1));
  EXPECT_FALSE(sh.IsInstanceOfType<Klass>(-1));
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_NULL), sh.GetTypeName(-1));
#if SQBIND_ASSERTS_ENABLED
  EXPECT_DEATH(sh.GetBool(-1), "");
  EXPECT_DEATH(sh.GetInteger(-1), "");
  EXPECT_DEATH(sh.GetFloat(-1), "");
  EXPECT_DEATH(sh.GetNumber<int32_t>(-1), "");
  EXPECT_DEATH(sh.GetNumber<float>(-1), "");
  EXPECT_DEATH(sh.GetNumber<double>(-1), "");
  EXPECT_DEATH(sh.GetString(-1), "");
  EXPECT_DEATH(sh.GetUserData(-1), "");
  EXPECT_DEATH(sh.GetUserDataAndTypeTag(1, nullptr), "");
  EXPECT_DEATH(sh.GetUserPointer(-1), "");
  EXPECT_DEATH(sh.GetInstance(-1), "");
  EXPECT_DEATH(sh.GetInstance<Klass>(-1), "");
  EXPECT_DEATH(sh.GetInstanceMatchTypeTag(1, nullptr), "");
  EXPECT_DEATH(sh.GetInstanceAsType<Klass>(-1), "");
#endif

  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestInteger)
{
  sqb::StackHandler sh(m_vm);

  // check integer type
  //
  ASSERT_NO_FATAL_FAILURE(PushObjectOfType(m_vm, OT_INTEGER));
  EXPECT_EQ(OT_INTEGER, sh.GetType(-1));
  EXPECT_TRUE(sh.IsNumber(-1));
  EXPECT_FALSE(sh.IsString(-1));
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_INTEGER), sh.GetTypeName(-1));
  EXPECT_EQ(kExpectedInteger, sh.GetInteger(-1));
  EXPECT_EQ(kExpectedInteger, sh.GetNumber<int32_t>(-1));
  EXPECT_FLOAT_EQ(static_cast<float>(kExpectedInteger), sh.GetFloat(-1));
  EXPECT_FLOAT_EQ(static_cast<float>(kExpectedInteger), sh.GetNumber<float>(-1));
  EXPECT_DOUBLE_EQ(static_cast<double>(kExpectedInteger), sh.GetNumber<double>(-1));
  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestFloat)
{
  sqb::StackHandler sh(m_vm);

  // check float type
  //
  ASSERT_NO_FATAL_FAILURE(PushObjectOfType(m_vm, OT_FLOAT));
  EXPECT_EQ(OT_FLOAT, sh.GetType(-1));
  EXPECT_TRUE(sh.IsNumber(-1));
  EXPECT_FALSE(sh.IsString(-1));
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_FLOAT), sh.GetTypeName(-1));
  EXPECT_EQ(static_cast<float>(kExpectedFloat), sh.GetInteger(-1));
  EXPECT_EQ(static_cast<float>(kExpectedFloat), sh.GetNumber<int32_t>(-1));
  EXPECT_FLOAT_EQ(kExpectedFloat, sh.GetFloat(-1));
  EXPECT_FLOAT_EQ(kExpectedFloat, sh.GetNumber<float>(-1));
  EXPECT_DOUBLE_EQ(static_cast<double>(kExpectedFloat), sh.GetNumber<double>(-1));
  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestBool)
{
  sqb::StackHandler sh(m_vm);

  // check bool type
  //
  ASSERT_NO_FATAL_FAILURE(PushObjectOfType(m_vm, OT_BOOL));
  EXPECT_EQ(OT_BOOL, sh.GetType(-1));
  EXPECT_FALSE(sh.IsNumber(-1));
  EXPECT_FALSE(sh.IsString(-1));
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_BOOL), sh.GetTypeName(-1));
  EXPECT_EQ(kExpectedBool, sh.GetBool(-1));
  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestString)
{
  sqb::StackHandler sh(m_vm);

  // check string type
  //
  ASSERT_NO_FATAL_FAILURE(PushObjectOfType(m_vm, OT_STRING));
  EXPECT_EQ(OT_STRING, sh.GetType(-1));
  EXPECT_FALSE(sh.IsNumber(-1));
  EXPECT_TRUE(sh.IsString(-1));
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_STRING), sh.GetTypeName(-1));
  EXPECT_STREQ(kExpectedString, sh.GetString(-1));
  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestTable)
{
  sqb::StackHandler sh(m_vm);

  // check table type
  //
  ASSERT_NO_FATAL_FAILURE(PushObjectOfType(m_vm, OT_TABLE));
  EXPECT_EQ(OT_TABLE, sh.GetType(-1));
  EXPECT_FALSE(sh.IsNumber(-1));
  EXPECT_FALSE(sh.IsString(-1));
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_TABLE), sh.GetTypeName(-1));
  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestArray)
{
  sqb::StackHandler sh(m_vm);

  // check array type
  //
  ASSERT_NO_FATAL_FAILURE(PushObjectOfType(m_vm, OT_ARRAY));
  EXPECT_EQ(OT_ARRAY, sh.GetType(-1));
  EXPECT_FALSE(sh.IsNumber(-1));
  EXPECT_FALSE(sh.IsString(-1));
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_ARRAY), sh.GetTypeName(-1));
  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestUserData)
{
  sqb::StackHandler sh(m_vm);

  // check userdata type
  //
  ASSERT_NO_FATAL_FAILURE(PushObjectOfType(m_vm, OT_USERDATA));
  EXPECT_EQ(OT_USERDATA, sh.GetType(-1));
  EXPECT_FALSE(sh.IsNumber(-1));
  EXPECT_FALSE(sh.IsString(-1));
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_USERDATA), sh.GetTypeName(-1));
  SQUserPointer userdata = nullptr;
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getuserdata(m_vm, 1, &userdata, nullptr));
  EXPECT_EQ(userdata, sh.GetUserData(-1));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_settypetag(m_vm, 1, &sh));
  SQUserPointer typetag = nullptr;
  EXPECT_EQ(userdata, sh.GetUserDataAndTypeTag(1, &typetag));
  EXPECT_EQ(&sh, typetag);
  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestClosure)
{
  sqb::StackHandler sh(m_vm);

  // check closure type
  //
  ASSERT_NO_FATAL_FAILURE(PushObjectOfType(m_vm, OT_CLOSURE));
  EXPECT_EQ(OT_CLOSURE, sh.GetType(-1));
  EXPECT_FALSE(sh.IsNumber(-1));
  EXPECT_FALSE(sh.IsString(-1));
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_CLOSURE), sh.GetTypeName(-1));
  sq_pop(m_vm, 1);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestNativeClosure)
{
  sqb::StackHandler sh(m_vm);

  // check native closure type
  //
  ASSERT_NO_FATAL_FAILURE(PushObjectOfType(m_vm, OT_NATIVECLOSURE));
  EXPECT_EQ(OT_NATIVECLOSURE, sh.GetType(-1));
  EXPECT_FALSE(sh.IsNumber(-1));
  EXPECT_FALSE(sh.IsString(-1));
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_NATIVECLOSURE), sh.GetTypeName(-1));
  sq_pop(m_vm, 1);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestGenerator)
{
  sqb::StackHandler sh(m_vm);

  // check generator type
  //
  ASSERT_NO_FATAL_FAILURE(PushObjectOfType(m_vm, OT_GENERATOR));
  EXPECT_EQ(OT_GENERATOR, sh.GetType(-1));
  EXPECT_FALSE(sh.IsNumber(-1));
  EXPECT_FALSE(sh.IsString(-1));
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_GENERATOR), sh.GetTypeName(-1));
  sq_pop(m_vm, 1);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestUserPointer)
{
  sqb::StackHandler sh(m_vm);

  // check user pointer type
  //
  ASSERT_NO_FATAL_FAILURE(PushObjectOfType(m_vm, OT_USERPOINTER));
  EXPECT_EQ(OT_USERPOINTER, sh.GetType(-1));
  EXPECT_FALSE(sh.IsNumber(-1));
  EXPECT_FALSE(sh.IsString(-1));
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_USERPOINTER), sh.GetTypeName(-1));
  EXPECT_EQ(kExpectedUserPointer, sh.GetUserPointer(-1));
  sq_pop(m_vm, 1);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestThread)
{
  sqb::StackHandler sh(m_vm);

  // check thread type
  //
  ASSERT_NO_FATAL_FAILURE(PushObjectOfType(m_vm, OT_THREAD));
  EXPECT_EQ(OT_THREAD, sh.GetType(-1));
  EXPECT_FALSE(sh.IsNumber(-1));
  EXPECT_FALSE(sh.IsString(-1));
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_THREAD), sh.GetTypeName(-1));
  sq_pop(m_vm, 1);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestClass)
{
  sqb::StackHandler sh(m_vm);

  // check class type
  //
  ASSERT_NO_FATAL_FAILURE(PushObjectOfType(m_vm, OT_CLASS));
  EXPECT_EQ(OT_CLASS, sh.GetType(-1));
  EXPECT_FALSE(sh.IsNumber(-1));
  EXPECT_FALSE(sh.IsString(-1));
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_CLASS), sh.GetTypeName(-1));
  sq_pop(m_vm, 1);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestInstance)
{
  sqb::StackHandler sh(m_vm);

  // check instance type
  //
  size_t size = sizeof(kExpectedUserPointer);
  (void)size;
  PushObjectOfType(m_vm, OT_INSTANCE);
  EXPECT_EQ(OT_INSTANCE, sh.GetType(-1));
  EXPECT_FALSE(sh.IsNumber(-1));
  EXPECT_FALSE(sh.IsString(-1));
  EXPECT_TRUE(sh.IsInstance(-1));
  EXPECT_FALSE(sh.IsInstanceOfType<Klass>(-1));
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_INSTANCE), sh.GetTypeName(-1));
  EXPECT_EQ(kExpectedInstance, sh.GetInstance(-1));
  EXPECT_EQ(kExpectedInstance, sh.GetInstance<Klass>(-1));
  EXPECT_EQ(kExpectedInstance, sh.GetInstanceMatchTypeTag(1, nullptr));
  EXPECT_EQ(nullptr, sh.GetInstanceMatchTypeTag(1, sqb::ClassTypeTag<Klass>::Get()));
  EXPECT_EQ(nullptr, sh.GetInstanceAsType<Klass>(-1));
  EXPECT_EQ(nullptr, sh.GetInstanceAsType<OtherKlass>(-1));

  // now check things with a ClassType typetag
  //
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getclass(m_vm, 1));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_settypetag(m_vm, -1, sqb::ClassTypeTag<Klass>::Get()));
  sq_pop(m_vm, 1);
  EXPECT_TRUE(sh.IsInstanceOfType<Klass>(-1));
  EXPECT_STREQ(_SC("Klass"), sh.GetTypeName(-1));
  EXPECT_EQ(kExpectedInstance, sh.GetInstanceMatchTypeTag(-1, sqb::ClassTypeTag<Klass>::Get()));
  EXPECT_EQ(nullptr, sh.GetInstanceMatchTypeTag(-1, sqb::ClassTypeTag<OtherKlass>::Get()));
  EXPECT_EQ(kExpectedInstance, sh.GetInstanceAsType<Klass>(-1));
  EXPECT_EQ(nullptr, sh.GetInstanceAsType<OtherKlass>(-1));

  // finally check that with a non ClassType typetag GetTypeName works as expected.
  //
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getclass(m_vm, 1));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_settypetag(m_vm, -1, &sh));
  sq_pop(m_vm, 1);
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_INSTANCE), sh.GetTypeName(-1));

  sq_pop(m_vm, 1);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestWeakRef)
{
  sqb::StackHandler sh(m_vm);

  // check weakref type
  //
  ASSERT_NO_FATAL_FAILURE(PushObjectOfType(m_vm, OT_WEAKREF));
  EXPECT_EQ(OT_WEAKREF, sh.GetType(-1));
  EXPECT_FALSE(sh.IsNumber(-1));
  EXPECT_FALSE(sh.IsString(-1));
  EXPECT_STREQ(sqb::RawTypeToTypeName(OT_WEAKREF), sh.GetTypeName(-1));
  sq_pop(m_vm, 1);
}

//----------------------------------------------------------------------------------------------------------------------
SQInteger NativeThrowNull(HSQUIRRELVM vm)
{
  sqb::StackHandler sh(vm);
  return sh.ThrowNull();
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestThrowNull)
{
  sq_newclosure(m_vm, &NativeThrowNull, 0);

  sq_pushroottable(m_vm);
  EXPECT_EQ(SQ_ERROR, sq_call(m_vm, 1, SQFalse, SQFalse));

  sq_getlasterror(m_vm);
  EXPECT_EQ(OT_NULL, sq_gettype(m_vm, -1));

  sq_pop(m_vm, 2);
}

//----------------------------------------------------------------------------------------------------------------------
SQInteger NativeThrowError(HSQUIRRELVM vm)
{
  sqb::StackHandler sh(vm);
  return sh.ThrowError(_SC("this is a test %s"), _SC("error"));
}

//----------------------------------------------------------------------------------------------------------------------
const SQChar* kLongErrorString =
  _SC("The polar bear (Ursus maritimus) is a bear native largely within the Arctic Circle encompassing the ")
  _SC("Arctic Ocean, its surrounding seas and surrounding land masses. It is the world's largest land carnivore ")
  _SC("and also the largest bear, together with the omnivorous Kodiak Bear, which is approximately the same size. ")
  _SC("An adult male weighs around 350-680 kg (770-1,500 lb),[4] while an adult female is about half that size. ")
  _SC("Although it is closely related to the brown bear, it has evolved to occupy a narrower ecological niche, ")
  _SC("with many body characteristics adapted for cold temperatures, for moving across snow, ice, and open water, ")
  _SC("and for hunting the seals which make up most of its diet.[5] Although most polar bears are born on land, ")
  _SC("they spend most of their time at sea. Their scientific name means \"maritime bear\", and derives from this ")
  _SC("fact. Polar bears can hunt consistently only from sea ice, hunting near the edge of the sea where seals are ")
  _SC("most common.");

//----------------------------------------------------------------------------------------------------------------------
SQInteger NativeThrowErrorLong(HSQUIRRELVM vm)
{
  sqb::StackHandler sh(vm);
  return sh.ThrowError(_SC("%s"), kLongErrorString);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerDeathTest, TestThrowError)
{
#if SQBIND_ASSERTS_ENABLED
  sqb::StackHandler sh(m_vm);
  EXPECT_DEATH(sh.ThrowError(nullptr), "");
#endif

  sq_newclosure(m_vm, &NativeThrowError, 0);

  sq_pushroottable(m_vm);
  EXPECT_EQ(SQ_ERROR, sq_call(m_vm, 1, SQFalse, SQFalse));

  sq_getlasterror(m_vm);

  const SQChar* error;
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, -1, &error));
  EXPECT_STREQ(_SC("this is a test error"), error);

  sq_pop(m_vm, 2);

  sq_newclosure(m_vm, &NativeThrowErrorLong, 0);

  sq_pushroottable(m_vm);
  EXPECT_EQ(SQ_ERROR, sq_call(m_vm, 1, SQFalse, SQFalse));

  sq_getlasterror(m_vm);

  EXPECT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, -1, &error));
  EXPECT_STREQ(kLongErrorString, error);

  sq_pop(m_vm, 2);
}

//----------------------------------------------------------------------------------------------------------------------
SQInteger NativeThrowParamCountError(HSQUIRRELVM vm)
{
  sqb::StackHandler sh(vm);
  return sh.ThrowParamCountError(2);
}

//----------------------------------------------------------------------------------------------------------------------
SQInteger NativeThrowParamCountErrorVariable(HSQUIRRELVM vm)
{
  sqb::StackHandler sh(vm);
  return sh.ThrowParamCountError(2, 2);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestThrowParamCountError)
{
  sq_newclosure(m_vm, &NativeThrowParamCountError, 0);

  sq_pushroottable(m_vm);
  EXPECT_EQ(SQ_ERROR, sq_call(m_vm, 1, SQFalse, SQFalse));

  sq_getlasterror(m_vm);

  const SQChar* error;
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, -1, &error));
  EXPECT_STREQ(_SC("wrong number of parameters '1' ; expected '2'"), error);

  sq_pop(m_vm, 2);

  sq_newclosure(m_vm, &NativeThrowParamCountErrorVariable, 0);

  sq_pushroottable(m_vm);
  EXPECT_EQ(SQ_ERROR, sq_call(m_vm, 1, SQFalse, SQFalse));

  sq_getlasterror(m_vm);

  EXPECT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, -1, &error));
  EXPECT_STREQ(_SC("too few parameters '1' ; expected at least '2'"), error);

  sq_poptop(m_vm);

  sq_pushroottable(m_vm);
  sq_pushnull(m_vm);
  sq_pushnull(m_vm);
  EXPECT_EQ(SQ_ERROR, sq_call(m_vm, 3, SQFalse, SQFalse));

  sq_getlasterror(m_vm);

  EXPECT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, -1, &error));
  EXPECT_STREQ(_SC("too many parameters '3' ; expected at most '2'"), error);

  sq_pop(m_vm, 2);
}

//----------------------------------------------------------------------------------------------------------------------
SQInteger NativeThrowParamErrorStringLiteral(HSQUIRRELVM vm)
{
  sqb::StackHandler sh(vm);
  return sh.ThrowParamError(1, _SC("string"));
}

//----------------------------------------------------------------------------------------------------------------------
SQInteger NativeThrowParamErrorSQObjectType(HSQUIRRELVM vm)
{
  sqb::StackHandler sh(vm);
  return sh.ThrowParamError(1, OT_STRING);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestThrowParamError)
{
  // generate the expected error message
  //
  sq_newclosure(m_vm, nullptr, 0);
  EXPECT_SQ_SUCCEEDED(m_vm, sq_setparamscheck(m_vm, 1, _SC("s")));
  sq_pushroottable(m_vm);
  EXPECT_EQ(SQ_ERROR, sq_call(m_vm, 1, SQFalse, SQFalse));

  sq_getlasterror(m_vm);

  const SQChar* error;
  ASSERT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, -1, &error));
  SQChar* expected = new SQChar[scstrlen(error) + 1];
  memcpy(expected, error, sizeof(SQChar) * (scstrlen(error) + 1));

  sq_pop(m_vm, 2);

  // test the string literal version of ThrowParamError
  //
  sq_newclosure(m_vm, &NativeThrowParamErrorStringLiteral, 0);

  sq_pushroottable(m_vm);
  EXPECT_EQ(SQ_ERROR, sq_call(m_vm, 1, SQFalse, SQFalse));

  sq_getlasterror(m_vm);

  EXPECT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, -1, &error));
  EXPECT_STREQ(expected, error);

  sq_pop(m_vm, 2);

  // test the SQObjectType version of ThrowParamError
  //
  sq_newclosure(m_vm, &NativeThrowParamErrorSQObjectType, 0);

  sq_pushroottable(m_vm);
  EXPECT_EQ(SQ_ERROR, sq_call(m_vm, 1, SQFalse, SQFalse));

  sq_getlasterror(m_vm);

  EXPECT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, -1, &error));
  EXPECT_STREQ(expected, error);

  sq_pop(m_vm, 2);

  delete [] expected;
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestReturn)
{
  SQInteger top = sq_gettop(m_vm);
  sqb::StackHandler sh(m_vm);

  EXPECT_EQ(0, sh.Return());
  EXPECT_EQ(top, sq_gettop(m_vm));

  {
    EXPECT_EQ(1, sh.Return(kExpectedBool));
    EXPECT_EQ(top + 1, sq_gettop(m_vm));
    SQBool actual;
    EXPECT_SQ_SUCCEEDED(m_vm, sq_getbool(m_vm, -1, &actual));
    EXPECT_EQ(static_cast<SQBool>(kExpectedBool ? SQTrue : SQFalse), actual);
    sq_poptop(m_vm);
  }

  {
    EXPECT_EQ(1, sh.Return(kExpectedInteger));
    EXPECT_EQ(top + 1, sq_gettop(m_vm));
    SQInteger actual;
    EXPECT_SQ_SUCCEEDED(m_vm, sq_getinteger(m_vm, -1, &actual));
    EXPECT_EQ(kExpectedInteger, actual);
    sq_poptop(m_vm);
  }

  {
    EXPECT_EQ(1, sh.Return(kExpectedFloat));
    EXPECT_EQ(top + 1, sq_gettop(m_vm));
    SQFloat actual;
    EXPECT_SQ_SUCCEEDED(m_vm, sq_getfloat(m_vm, -1, &actual));
    EXPECT_EQ(kExpectedFloat, actual);
    sq_poptop(m_vm);
  }

  {
    EXPECT_EQ(1, sh.Return(kExpectedString));
    EXPECT_EQ(top + 1, sq_gettop(m_vm));
    const SQChar* actual;
    EXPECT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, -1, &actual));
    EXPECT_STREQ(kExpectedString, actual);
    sq_poptop(m_vm);
  }

  {
    EXPECT_EQ(1, sh.Return(kExpectedUserPointer));
    EXPECT_EQ(top + 1, sq_gettop(m_vm));
    SQUserPointer actual;
    EXPECT_SQ_SUCCEEDED(m_vm, sq_getuserpointer(m_vm, -1, &actual));
    EXPECT_EQ(kExpectedUserPointer, actual);
    sq_poptop(m_vm);
  }

  {
    sq_pushinteger(m_vm, kExpectedInteger);
    HSQOBJECT object;
    EXPECT_SQ_SUCCEEDED(m_vm, sq_getstackobj(m_vm, -1, &object));
    sq_addref(m_vm, &object);
    sq_poptop(m_vm);

    EXPECT_EQ(1, sh.Return(object));
    EXPECT_EQ(top + 1, sq_gettop(m_vm));
    SQInteger actual;
    EXPECT_SQ_SUCCEEDED(m_vm, sq_getinteger(m_vm, -1, &actual));
    EXPECT_EQ(kExpectedInteger, actual);
    sq_poptop(m_vm);

    sq_release(m_vm, &object);
  }

  {
    OtherKlass instance;
    const OtherKlass const_instance;
    const SQChar* error;

    EXPECT_SQ_FAILED(sh.Return(&instance));
    EXPECT_EQ(top, sq_gettop(m_vm));
    sq_getlasterror(m_vm);
    EXPECT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, -1, &error));
    EXPECT_STREQ(_SC("error returning instance of type 'OtherKlass'"), error);
    sq_poptop(m_vm);
    sq_reseterror(m_vm);

    EXPECT_SQ_FAILED(sh.Return(instance));
    EXPECT_EQ(top, sq_gettop(m_vm));
    sq_getlasterror(m_vm);
    EXPECT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, -1, &error));
    EXPECT_STREQ(_SC("error returning instance of type 'OtherKlass'"), error);
    sq_poptop(m_vm);
    sq_reseterror(m_vm);

    EXPECT_SQ_FAILED(sh.Return(&const_instance));
    EXPECT_EQ(top, sq_gettop(m_vm));
    sq_getlasterror(m_vm);
    EXPECT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, -1, &error));
    EXPECT_STREQ(_SC("error returning instance of type 'OtherKlass'"), error);
    sq_poptop(m_vm);
    sq_reseterror(m_vm);

    EXPECT_SQ_FAILED(sh.Return(const_instance));
    EXPECT_EQ(top, sq_gettop(m_vm));
    sq_getlasterror(m_vm);
    EXPECT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, -1, &error));
    EXPECT_STREQ(_SC("error returning instance of type 'OtherKlass'"), error);
    sq_poptop(m_vm);
    sq_reseterror(m_vm);
  }

  {
    Klass instance;
    const Klass const_instance;
    EXPECT_EQ(1, sh.Return(&instance));
    EXPECT_EQ(top + 1, sq_gettop(m_vm));
    EXPECT_EQ(OT_USERPOINTER, sq_gettype(m_vm, -1));
    sq_poptop(m_vm);

    EXPECT_EQ(1, sh.Return(instance));
    EXPECT_EQ(top + 1, sq_gettop(m_vm));
    EXPECT_EQ(OT_USERPOINTER, sq_gettype(m_vm, -1));
    sq_poptop(m_vm);

    EXPECT_EQ(1, sh.Return(&const_instance));
    EXPECT_EQ(top + 1, sq_gettop(m_vm));
    EXPECT_EQ(OT_USERPOINTER, sq_gettype(m_vm, -1));
    sq_poptop(m_vm);

    EXPECT_EQ(1, sh.Return(const_instance));
    EXPECT_EQ(top + 1, sq_gettop(m_vm));
    EXPECT_EQ(OT_USERPOINTER, sq_gettype(m_vm, -1));
    sq_poptop(m_vm);
  }
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestReturnWithReleaseHook)
{
  Klass instance;
  const SQChar* error;

  SQInteger top = sq_gettop(m_vm);
  sqb::StackHandler sh(m_vm);

  EXPECT_EQ(1, sh.Return<Klass>(nullptr, nullptr));
  EXPECT_EQ(top + 1, sq_gettop(m_vm));
  EXPECT_EQ(OT_NULL, sq_gettype(m_vm, -1));
  sq_poptop(m_vm);

  EXPECT_EQ(SQ_ERROR, sh.Return<Klass>(&instance, nullptr));
  EXPECT_EQ(top, sq_gettop(m_vm));
  sq_getlasterror(m_vm);
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, -1, &error));
  EXPECT_STREQ(_SC("error returning instance of type 'Klass'"), error);
  sq_poptop(m_vm);
  sq_reseterror(m_vm);

  ::testing::StrictMock<MockReleaseHook> mock;
  MockReleaseHook::m_instance = &mock;

  EXPECT_CALL(mock, ClassReleaseHook(&instance, sizeof(Klass)))
    .Times(1);

  EXPECT_EQ(SQ_ERROR, sh.Return<Klass>(&instance, &MockReleaseHook::ReleaseHook));
  EXPECT_EQ(top, sq_gettop(m_vm));
  sq_getlasterror(m_vm);
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, -1, &error));
  EXPECT_STREQ(_SC("error returning instance of type 'Klass'"), error);
  sq_poptop(m_vm);
  sq_reseterror(m_vm);

  EXPECT_SQ_SUCCEEDED(m_vm, sq_newclass(m_vm, SQFalse));
  HSQOBJECT klass;
  sq_getstackobj(m_vm, -1, &klass);
  sqb::ClassTypeTag<Klass>::Get()->SetClassObject(m_vm, klass);
  sq_poptop(m_vm);

  EXPECT_EQ(1, sh.Return<Klass>(&instance, nullptr));
  EXPECT_EQ(top + 1, sq_gettop(m_vm));
  EXPECT_EQ(OT_INSTANCE, sq_gettype(m_vm, -1));
  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
SQInteger NativeSuspendVM(HSQUIRRELVM vm)
{
  sqb::StackHandler sh(vm);
  return sh.SuspendVM();
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(StackHandlerTest, TestSuspendVM)
{
  sq_pushroottable(m_vm);
  sq_pushstring(m_vm, _SC("func"), -1);
  sq_newclosure(m_vm, &NativeSuspendVM, 0);
  EXPECT_SQ_SUCCEEDED(m_vm, sq_newslot(m_vm, -3, SQFalse));
  sq_poptop(m_vm);

  const SQChar* buffer = _SC("func(); return 0;");
  EXPECT_SQ_SUCCEEDED(m_vm, sq_compilebuffer(m_vm, buffer, scstrlen(buffer), _SC("buffer"), SQFalse));

  sq_pushroottable(m_vm);
  EXPECT_SQ_SUCCEEDED(m_vm, sq_call(m_vm, 1, SQFalse, SQFalse));
  EXPECT_EQ(SQ_VMSTATE_SUSPENDED, sq_getvmstate(m_vm));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_wakeupvm(m_vm, SQFalse, SQFalse, SQFalse, SQFalse));

  // as it has been suspended the root table was not automatically popped from the stack
  //
  sq_pop(m_vm, 2);
}
