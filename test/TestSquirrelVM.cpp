//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <gmock/gmock.h>
//----------------------------------------------------------------------------------------------------------------------
#include <sqbind/sqbSquirrelObject.h>
#include <sqbind/sqbSquirrelVM.h>
//----------------------------------------------------------------------------------------------------------------------
#include "fixtures/SquirrelVMFixture.h"
//----------------------------------------------------------------------------------------------------------------------

typedef SquirrelVMFixture SquirrelVMTest;

//----------------------------------------------------------------------------------------------------------------------
TEST_F(SquirrelVMTest, TestSetGetAndCast)
{
  sqb::SquirrelVM vm;

  EXPECT_EQ(nullptr, vm.SetVM(m_vm));

  EXPECT_EQ(m_vm, static_cast<HSQUIRRELVM>(vm));
  EXPECT_EQ(m_vm, vm.GetVM());

  HSQUIRRELVM other = sq_open(128);

  EXPECT_EQ(m_vm, vm.SetVM(other));

  EXPECT_NE(m_vm, static_cast<HSQUIRRELVM>(vm));
  EXPECT_NE(m_vm, vm.GetVM());

  EXPECT_EQ(other, static_cast<HSQUIRRELVM>(vm));
  EXPECT_EQ(other, vm.GetVM());

  EXPECT_EQ(other, vm.SetVM(nullptr));

  sq_close(other);

  EXPECT_EQ(nullptr, static_cast<HSQUIRRELVM>(vm));
  EXPECT_EQ(nullptr, vm.GetVM());
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(SquirrelVMTest, TestInitialiseShutdown)
{
  // test some basic initialise and shutdown functionality
  //
  {
    sqb::SquirrelVM vm;
    EXPECT_FALSE(vm.Shutdown());

    EXPECT_TRUE(vm.Initialise());
    EXPECT_FALSE(vm.Initialise());

    EXPECT_TRUE(vm.Shutdown());
    EXPECT_FALSE(vm.Shutdown());
  }

  // test initialising with sqb::InitialisationOptions::kRegisterNoStdLibs
  //
  {
    sqb::SquirrelVM vm;
    EXPECT_TRUE(vm.Initialise(sqb::InitialisationOptions::kRegisterNoStdLibs));
    EXPECT_FALSE(IsStdBlobLibRegistered(vm));
    EXPECT_FALSE(IsStdIOLibRegistered(vm));
    EXPECT_FALSE(IsStdMathLibRegistered(vm));
    EXPECT_FALSE(IsStdStringLibRegistered(vm));
    EXPECT_FALSE(IsStdSystemLibRegistered(vm));
    EXPECT_TRUE(vm.Shutdown());
  }

  // test initialising with sqb::InitialisationOptions::kRegisterStdBlobLib
  //
  {
    sqb::SquirrelVM vm;
    EXPECT_TRUE(vm.Initialise(sqb::InitialisationOptions::kRegisterStdBlobLib));
    EXPECT_TRUE(IsStdBlobLibRegistered(vm));
    EXPECT_FALSE(IsStdIOLibRegistered(vm));
    EXPECT_FALSE(IsStdMathLibRegistered(vm));
    EXPECT_FALSE(IsStdStringLibRegistered(vm));
    EXPECT_FALSE(IsStdSystemLibRegistered(vm));
    EXPECT_TRUE(vm.Shutdown());
  }

  // test initialising with sqb::InitialisationOptions::kRegisterStdIOLib
  //
  {
    sqb::SquirrelVM vm;
    EXPECT_TRUE(vm.Initialise(sqb::InitialisationOptions::kRegisterStdIOLib));
    EXPECT_FALSE(IsStdBlobLibRegistered(vm));
    EXPECT_TRUE(IsStdIOLibRegistered(vm));
    EXPECT_FALSE(IsStdMathLibRegistered(vm));
    EXPECT_FALSE(IsStdStringLibRegistered(vm));
    EXPECT_FALSE(IsStdSystemLibRegistered(vm));
    EXPECT_TRUE(vm.Shutdown());
  }

  // test initialising with sqb::InitialisationOptions::kRegisterStdMathLib
  //
  {
    sqb::SquirrelVM vm;
    EXPECT_TRUE(vm.Initialise(sqb::InitialisationOptions::kRegisterStdMathLib));
    EXPECT_FALSE(IsStdBlobLibRegistered(vm));
    EXPECT_FALSE(IsStdIOLibRegistered(vm));
    EXPECT_TRUE(IsStdMathLibRegistered(vm));
    EXPECT_FALSE(IsStdStringLibRegistered(vm));
    EXPECT_FALSE(IsStdSystemLibRegistered(vm));
    EXPECT_TRUE(vm.Shutdown());
  }

  // test initialising with sqb::InitialisationOptions::kRegisterStdStringLib
  //
  {
    sqb::SquirrelVM vm;
    EXPECT_TRUE(vm.Initialise(sqb::InitialisationOptions::kRegisterStdStringLib));
    EXPECT_FALSE(IsStdBlobLibRegistered(vm));
    EXPECT_FALSE(IsStdIOLibRegistered(vm));
    EXPECT_FALSE(IsStdMathLibRegistered(vm));
    EXPECT_TRUE(IsStdStringLibRegistered(vm));
    EXPECT_FALSE(IsStdSystemLibRegistered(vm));
    EXPECT_TRUE(vm.Shutdown());
  }

  // test initialising with sqb::InitialisationOptions::kRegisterStdSystemLib
  //
  {
    sqb::SquirrelVM vm;
    EXPECT_TRUE(vm.Initialise(sqb::InitialisationOptions::kRegisterStdSystemLib));
    EXPECT_FALSE(IsStdBlobLibRegistered(vm));
    EXPECT_FALSE(IsStdIOLibRegistered(vm));
    EXPECT_FALSE(IsStdMathLibRegistered(vm));
    EXPECT_FALSE(IsStdStringLibRegistered(vm));
    EXPECT_TRUE(IsStdSystemLibRegistered(vm));
    EXPECT_TRUE(vm.Shutdown());
  }

  // test initialising with sqb::InitialisationOptions::kRegisterAllStdLibs
  //
  {
    sqb::SquirrelVM vm;
    EXPECT_TRUE(vm.Initialise(sqb::InitialisationOptions::kRegisterAllStdLibs));
    EXPECT_TRUE(IsStdBlobLibRegistered(vm));
    EXPECT_TRUE(IsStdIOLibRegistered(vm));
    EXPECT_TRUE(IsStdMathLibRegistered(vm));
    EXPECT_TRUE(IsStdStringLibRegistered(vm));
    EXPECT_TRUE(IsStdSystemLibRegistered(vm));
    EXPECT_TRUE(vm.Shutdown());
  }
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(SquirrelVMTest, TestExecuteBuffer)
{
  sqb::SquirrelVM vm(m_vm);

  sqb::CompilationOptions compileOptions;
  compileOptions.m_sourceName = nullptr;
  compileOptions.m_printError = false;

  sqb::ExecutionOptions executeOptions;
  executeOptions.m_raiseError = false;

  EXPECT_SQ_SUCCEEDED(m_vm, vm.ExecuteBuffer(_SC("return 10"), compileOptions, executeOptions));

  sqb::SquirrelObject returnValue;
  EXPECT_SQ_SUCCEEDED(m_vm, vm.ExecuteBuffer(_SC("return 10"), compileOptions, executeOptions, &returnValue));

  sq_pushobject(m_vm, returnValue);
  EXPECT_EQ(OT_INTEGER, sq_gettype(vm, -1));

  SQInteger actual;
  EXPECT_SQ_SUCCEEDED(vm, sq_getinteger(vm, -1, &actual));
  EXPECT_EQ(10, actual);

  sq_poptop(vm);
}
