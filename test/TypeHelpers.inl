#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
// InvalidTypeInfoClass
//----------------------------------------------------------------------------------------------------------------------
namespace sqb
{
//----------------------------------------------------------------------------------------------------------------------
template<>
struct TypeInfo<InvalidTypeInfoClass>
{
  enum
  {
    kTypeID = 0xdeadbeef,
    kTypeSize = -1,
    kTypeMask = '\n',
    kTypeIsInstance = SQTrue,
  };
  const SQChar *m_typeName;
  inline TypeInfo()
    : m_typeName(_SC(""))
  {
  }
  inline operator ScriptVarType() const
  {
    return static_cast<ScriptVarType>(kTypeID);
  }
};

//----------------------------------------------------------------------------------------------------------------------
template<>
inline SQRESULT Push(HSQUIRRELVM vm, InvalidStackUtilsClass *SQBIND_UNUSED(value))
{
  return sq_throwerror(vm, _SC("error returning object of type 'InvalidStackUtilsClass'"));
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline SQRESULT Push(HSQUIRRELVM vm, InvalidStackUtilsClass &SQBIND_UNUSED(value))
{
  return sq_throwerror(vm, _SC("error returning object of type 'InvalidStackUtilsClass'"));
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline SQRESULT Push(HSQUIRRELVM vm, const InvalidStackUtilsClass *SQBIND_UNUSED(value))
{
  return sq_throwerror(vm, _SC("error returning object of type 'InvalidStackUtilsClass'"));
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline SQRESULT Push(HSQUIRRELVM vm, const InvalidStackUtilsClass &SQBIND_UNUSED(value))
{
  return sq_throwerror(vm, _SC("error returning object of type 'InvalidStackUtilsClass'"));
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline bool Match(
  TypeWrapper<InvalidStackUtilsClass *> SQBIND_UNUSED(wrapper),
  HSQUIRRELVM SQBIND_UNUSED(vm),
  SQInteger SQBIND_UNUSED(index))
{
  return false;
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline bool Match(
  TypeWrapper<InvalidStackUtilsClass &> SQBIND_UNUSED(wrapper),
  HSQUIRRELVM SQBIND_UNUSED(vm),
  SQInteger SQBIND_UNUSED(index))
{
  return false;
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline bool Match(
  TypeWrapper<const InvalidStackUtilsClass *> SQBIND_UNUSED(wrapper),
  HSQUIRRELVM SQBIND_UNUSED(vm),
  SQInteger SQBIND_UNUSED(index))
{
  return false;
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline bool Match(
  TypeWrapper<const InvalidStackUtilsClass &> SQBIND_UNUSED(wrapper),
  HSQUIRRELVM SQBIND_UNUSED(vm),
  SQInteger SQBIND_UNUSED(index))
{
  return false;
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline InvalidStackUtilsClass *Get(
  TypeWrapper<InvalidStackUtilsClass *> SQBIND_UNUSED(wrapper),
  HSQUIRRELVM SQBIND_UNUSED(vm),
  SQInteger SQBIND_UNUSED(index))
{
  return nullptr;
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline InvalidStackUtilsClass &Get(
  TypeWrapper<InvalidStackUtilsClass &>,
  HSQUIRRELVM SQBIND_UNUSED(vm),
  SQInteger SQBIND_UNUSED(index))
{
  InvalidStackUtilsClass *result = nullptr;
  return *result;
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline const InvalidStackUtilsClass *Get(
  TypeWrapper<const InvalidStackUtilsClass *> SQBIND_UNUSED(wrapper),
  HSQUIRRELVM SQBIND_UNUSED(vm),
  SQInteger SQBIND_UNUSED(index))
{
  return nullptr;
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline const InvalidStackUtilsClass &Get(
  TypeWrapper<const InvalidStackUtilsClass &> SQBIND_UNUSED(wrapper),
  HSQUIRRELVM SQBIND_UNUSED(vm),
  SQInteger SQBIND_UNUSED(index))
{
  InvalidStackUtilsClass *result = nullptr;
  return *result;
}

} // namespace sqb
