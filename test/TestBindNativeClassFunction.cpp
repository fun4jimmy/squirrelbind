//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <gmock/gmock.h>

#include <sqbind/sqbBind.h>

#include "fixtures/SquirrelFixture.h"
#include "mocks/MockNativeFunction.h"
#include "TypeHelpers.h"
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
typedef SquirrelFixture BindNativeClassFunctionTest;

//----------------------------------------------------------------------------------------------------------------------
TEST_F(BindNativeClassFunctionTest, TestInvalidParameters)
{
  // test wrong objects on the stack fails
  //
  EXPECT_FALSE(sqb::Bind::BindNativeClassFunction(m_vm, 1, &MockNativeFunction::NativeClassFunction, _SC("test_positive_index")));

  sq_pushroottable(m_vm);
  EXPECT_FALSE(sqb::Bind::BindNativeClassFunction(m_vm, 1, &MockNativeFunction::NativeClassFunction, _SC("native")));
  sq_poptop(m_vm);

  EXPECT_SQ_SUCCEEDED(m_vm, sq_newclass(m_vm, SQFalse));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_settypetag(m_vm, -1, sqb::ClassTypeTag<MockNativeFunction>::Get()));

  // test invalid name fails
  //
  EXPECT_FALSE(sqb::Bind::BindNativeClassFunction(m_vm, 1, &MockNativeFunction::NativeClassFunction, nullptr));
  EXPECT_FALSE(sqb::Bind::BindNativeClassFunction(m_vm, 1, &MockNativeFunction::NativeClassFunction, _SC("")));

  // test valid arguments succeed
  //
  ASSERT_TRUE(sqb::Bind::BindNativeClassFunction(m_vm, 1, &MockNativeFunction::NativeClassFunction, _SC("positive")));
  ASSERT_TRUE(sqb::Bind::BindNativeClassFunction(m_vm, -1, &MockNativeFunction::NativeClassFunction, _SC("negative")));

  ::testing::StrictMock<MockNativeFunction> mock;
  MockNativeFunction::m_instance = &mock;

  EXPECT_CALL(mock, NativeClassFunction(m_vm))
    .Times(2);

  sq_pushroottable(m_vm);
  sq_pushstring(m_vm, _SC("mock"), -1);
  EXPECT_SQ_SUCCEEDED(m_vm, sq_createinstance(m_vm, -3));
  // this cast is very important as there is a pointer offset between the StrictMock and the class itself
  //
  EXPECT_SQ_SUCCEEDED(m_vm, sq_setinstanceup(m_vm, -1, static_cast<MockNativeFunction*>(&mock)));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_rawset(m_vm, -3));
  sq_poptop(m_vm);

  CompileAndSucceedCall(_SC("mock.positive()"));
  CompileAndSucceedCall(_SC("mock.negative()"));

  // pop class from stack
  //
  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(BindNativeClassFunctionTest, TestParamCheck)
{
  EXPECT_SQ_SUCCEEDED(m_vm, sq_newclass(m_vm, SQFalse));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_settypetag(m_vm, -1, sqb::ClassTypeTag<MockNativeFunction>::Get()));

  ::testing::StrictMock<MockNativeFunction> mock;
  MockNativeFunction::m_instance = &mock;

  sq_pushroottable(m_vm);
  sq_pushstring(m_vm, _SC("mock"), -1);
  EXPECT_SQ_SUCCEEDED(m_vm, sq_createinstance(m_vm, -3));
  // this cast is very important as there is a pointer offset between the StrictMock and the class itself
  //
  EXPECT_SQ_SUCCEEDED(m_vm, sq_setinstanceup(m_vm, -1, static_cast<MockNativeFunction*>(&mock)));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_rawset(m_vm, -3));
  sq_poptop(m_vm);

  EXPECT_CALL(mock, NativeClassFunction(m_vm))
    .Times(3);

  // test invalid typemask fails
  //
  {
    sqb::FunctionOptions options;
    options.ParamCheckCount(5);
    options.TypeMask(_SC("hello"));
    EXPECT_FALSE(sqb::Bind::BindNativeClassFunction(m_vm, -1, &MockNativeFunction::NativeClassFunction, _SC("native"), options));
  }

  // test half valid typemask does not set
  //
  {
    sqb::FunctionOptions options;
    options.ParamCheckCount(3);
    options.TypeMask(nullptr);
    ASSERT_TRUE(sqb::Bind::BindNativeClassFunction(m_vm, -1, &MockNativeFunction::NativeClassFunction, _SC("native"), options));
  }
  CompileAndSucceedCall(_SC("mock.native()"));

  {
    sqb::FunctionOptions options;
    options.TypeMask(_SC(".s"));
    ASSERT_TRUE(sqb::Bind::BindNativeClassFunction(m_vm, -1, &MockNativeFunction::NativeClassFunction, _SC("native"), options));
  }
  CompileAndSucceedCall(_SC("mock.native()"));

  // test valid typemask sets
  //
  {
    sqb::FunctionOptions options;
    options.ParamCheckCount(2);
    options.TypeMask(_SC(".s"));
    ASSERT_TRUE(sqb::Bind::BindNativeClassFunction(m_vm, -1, &MockNativeFunction::NativeClassFunction, _SC("native"), options));
  }

  CompileAndFailCall(_SC("mock.native()"));
  CompileAndSucceedCall(_SC("mock.native(\"string\")"));

  // pop class off stack
  //
  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(BindNativeClassFunctionTest, TestFreeVariables)
{
  EXPECT_SQ_SUCCEEDED(m_vm, sq_newclass(m_vm, SQFalse));

  sqb::FunctionOptions options;
  options.FreeVariables(2);

  // free variables are bound backwards so the last in the stack will be the first free variable for the function.
  //
  sq_pushstring(m_vm, kExpectedString, -1);
  sq_pushinteger(m_vm, kExpectedInteger);
  ASSERT_TRUE(sqb::Bind::BindNativeClassFunction(m_vm, -3, &MockNativeFunction::NativeClassFunction, _SC("test"), options));

  sq_pushstring(m_vm, _SC("test"), -1);
  EXPECT_SQ_SUCCEEDED(m_vm, sq_rawget(m_vm, -2));

  SQUnsignedInteger parameterCount = 0;
  SQUnsignedInteger freeVariableCount = 0;
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getclosureinfo(m_vm, -1, &parameterCount, &freeVariableCount));

  EXPECT_EQ(0u, parameterCount);
  EXPECT_GE(freeVariableCount, 2u);

  sq_pop(m_vm, 2);
}
