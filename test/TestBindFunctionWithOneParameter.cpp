//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the 'Software'), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <gmock/gmock.h>

#include <sqbind/sqbBind.h>

#include "fixtures/SquirrelFixture.h"
#include "mocks/MockFunctionWithOneParameter.h"
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
typedef SquirrelFixture BindFunctionTest;

//----------------------------------------------------------------------------------------------------------------------
typedef SquirrelFixture BindClassFunctionTest;

//----------------------------------------------------------------------------------------------------------------------
typedef SquirrelFixture BindSingletonFunctionTest;

//----------------------------------------------------------------------------------------------------------------------
// TestOneParameterReturnVoid
//----------------------------------------------------------------------------------------------------------------------
TEST_F(BindFunctionTest, TestOneParameterReturnVoid)
{
  ::testing::StrictMock<MockFunctionWithOneParameter> mock;
  MockFunctionWithOneParameter::m_instance = &mock;

  sq_pushroottable(m_vm);
  sqb::Bind::BindClass<BoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("BoundClass"));
  sqb::Bind::BindClass<OtherBoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("OtherBoundClass"));
  sq_poptop(m_vm);

  OtherBoundClass invalidParameter;
  BoundClass parameter0(0);

  EXPECT_CALL(mock, ClassVoidFunction(parameter0))
    .Times(2);

  // bind the objects required for calling the function
  //
  sq_pushroottable(m_vm);

  EXPECT_TRUE(sqb::Bind::BindFunction(m_vm, -1, &MockFunctionWithOneParameter::VoidFunction, _SC("VoidFunction")));

  sq_pushstring(m_vm, _SC("Mock"), -1);
  EXPECT_SQ_SUCCEEDED(m_vm, sq_newclass(m_vm, SQFalse));
  EXPECT_TRUE(sqb::Bind::BindFunction(m_vm, 3, &MockFunctionWithOneParameter::VoidFunction, _SC("VoidFunction")));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_rawset(m_vm, -3));

  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, &invalidParameter, _SC("invalidParameter")));
  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, &parameter0, _SC("parameter0")));
  sq_poptop(m_vm);

  CompileAndSucceedCall(_SC("VoidFunction(parameter0)"));
  CompileAndSucceedCall(_SC("Mock.VoidFunction(parameter0)"));

  CompileAndFailCall(_SC("VoidFunction(invalidParameter)"));
  CheckErrorString(_SC("parameter 1 has an invalid type 'OtherBoundClass' ; expected: 'BoundClass'"));
}

//----------------------------------------------------------------------------------------------------------------------
// TestOneParameterReturnVoid
//----------------------------------------------------------------------------------------------------------------------
TEST_F(BindClassFunctionTest, TestOneParameterReturnVoid)
{
  ::testing::StrictMock<MockFunctionWithOneParameter> mock;
  MockFunctionWithOneParameter::m_instance = &mock;

  sq_pushroottable(m_vm);
  sqb::Bind::BindClass<BoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("BoundClass"));
  sqb::Bind::BindClass<OtherBoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("OtherBoundClass"));
  sq_poptop(m_vm);

  OtherBoundClass invalidParameter;
  BoundClass parameter0(0);

  EXPECT_CALL(mock, ClassVoidFunction(parameter0))
    .Times(1);
  EXPECT_CALL(mock, ConstClassVoidFunction(parameter0))
    .Times(1);

  // bind the objects required for calling the function
  //
  sq_pushroottable(m_vm);

  // bind the mock class
  //
  EXPECT_SQ_SUCCEEDED(m_vm, sq_newclass(m_vm, SQFalse));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_settypetag(m_vm, -1, sqb::ClassTypeTag<MockFunctionWithOneParameter>::Get()));
  HSQOBJECT class_object;
  sq_getstackobj(m_vm, -1, &class_object);
  sqb::ClassTypeTag<MockFunctionWithOneParameter>::Get()->SetClassObject(m_vm, class_object);
  EXPECT_TRUE(sqb::Bind::BindClassFunction<MockFunctionWithOneParameter>(m_vm, -1, &MockFunctionWithOneParameter::ClassVoidFunction, _SC("ClassVoidFunction")));
  EXPECT_TRUE(sqb::Bind::BindClassFunction<MockFunctionWithOneParameter>(m_vm, 2, &MockFunctionWithOneParameter::ConstClassVoidFunction, _SC("ConstClassVoidFunction")));
  sq_poptop(m_vm);

  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, static_cast<MockFunctionWithOneParameter *>(&mock), _SC("mock")));

  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, &invalidParameter, _SC("invalidParameter")));
  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, &parameter0, _SC("parameter0")));
  sq_poptop(m_vm);

  CompileAndSucceedCall(_SC("mock.ClassVoidFunction(parameter0)"));
  CompileAndSucceedCall(_SC("mock.ConstClassVoidFunction(parameter0)"));

  CompileAndFailCall(_SC("mock.ClassVoidFunction(invalidParameter)"));
  CheckErrorString(_SC("parameter 1 has an invalid type 'OtherBoundClass' ; expected: 'BoundClass'"));
  CompileAndFailCall(_SC("mock.ConstClassVoidFunction(invalidParameter)"));
  CheckErrorString(_SC("parameter 1 has an invalid type 'OtherBoundClass' ; expected: 'BoundClass'"));
}

//----------------------------------------------------------------------------------------------------------------------
// TestOneParameterReturnVoid
//----------------------------------------------------------------------------------------------------------------------
TEST_F(BindSingletonFunctionTest, TestOneParameterReturnVoid)
{
  ::testing::StrictMock<MockFunctionWithOneParameter> mock;
  MockFunctionWithOneParameter::m_instance = &mock;

  sq_pushroottable(m_vm);
  sqb::Bind::BindClass<BoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("BoundClass"));
  sqb::Bind::BindClass<OtherBoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("OtherBoundClass"));
  sq_poptop(m_vm);

  OtherBoundClass invalidParameter;
  BoundClass parameter0(0);

  EXPECT_CALL(mock, ClassVoidFunction(parameter0))
    .Times(1);
  EXPECT_CALL(mock, ConstClassVoidFunction(parameter0))
    .Times(1);

  // bind the objects required for calling the function
  //
  sq_pushroottable(m_vm);

  // bind the mock class
  //
  sq_pushstring(m_vm, _SC("Mock"), -1);
  EXPECT_SQ_SUCCEEDED(m_vm, sq_newclass(m_vm, SQFalse));
  EXPECT_TRUE(sqb::Bind::BindSingletonFunction<MockFunctionWithOneParameter>(m_vm, -1, &mock, &MockFunctionWithOneParameter::ClassVoidFunction, _SC("ClassVoidFunction")));
  EXPECT_TRUE(sqb::Bind::BindSingletonFunction<MockFunctionWithOneParameter>(m_vm, 1, &mock, &MockFunctionWithOneParameter::ConstClassVoidFunction, _SC("ConstClassVoidFunction")));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_rawset(m_vm, -3));

  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, &invalidParameter, _SC("invalidParameter")));
  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, &parameter0, _SC("parameter0")));
  sq_poptop(m_vm);

  CompileAndSucceedCall(_SC("Mock.ClassVoidFunction(parameter0)"));
  CompileAndSucceedCall(_SC("ConstClassVoidFunction(parameter0)"));

  CompileAndFailCall(_SC("Mock.ClassVoidFunction(invalidParameter)"));
  CheckErrorString(_SC("parameter 1 has an invalid type 'OtherBoundClass' ; expected: 'BoundClass'"));
  CompileAndFailCall(_SC("ConstClassVoidFunction(invalidParameter)"));
  CheckErrorString(_SC("parameter 1 has an invalid type 'OtherBoundClass' ; expected: 'BoundClass'"));
}

//----------------------------------------------------------------------------------------------------------------------
// TestOneParameter
//----------------------------------------------------------------------------------------------------------------------
TEST_F(BindFunctionTest, TestOneParameter)
{
  ::testing::StrictMock<MockFunctionWithOneParameter> mock;
  MockFunctionWithOneParameter::m_instance = &mock;

  sq_pushroottable(m_vm);
  sqb::Bind::BindClass<BoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("BoundClass"));
  sqb::Bind::BindClass<OtherBoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("OtherBoundClass"));
  sq_poptop(m_vm);

  OtherBoundClass invalidParameter;
  BoundClass parameter0(0);

  BoundClass expected_result(0xffffffff);
  EXPECT_CALL(mock, ClassFunction(parameter0))
    .Times(2)
    .WillRepeatedly(::testing::Return(expected_result));

  // bind the objects required for calling the function
  //
  sq_pushroottable(m_vm);

  EXPECT_TRUE(sqb::Bind::BindFunction(m_vm, -1, &MockFunctionWithOneParameter::Function, _SC("Function")));

  sq_pushstring(m_vm, _SC("Mock"), -1);
  EXPECT_SQ_SUCCEEDED(m_vm, sq_newclass(m_vm, SQFalse));
  EXPECT_TRUE(sqb::Bind::BindFunction(m_vm, 3, &MockFunctionWithOneParameter::Function, _SC("Function")));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_rawset(m_vm, -3));

  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, &invalidParameter, _SC("invalidParameter")));
  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, &parameter0, _SC("parameter0")));
  sq_poptop(m_vm);

  BoundClass actual_result = CompileAndCallReturnResult<BoundClass>(_SC("return Function(parameter0)"));
  EXPECT_EQ(expected_result, actual_result);
  actual_result = CompileAndCallReturnResult<BoundClass>(_SC("return Mock.Function(parameter0)"));
  EXPECT_EQ(expected_result, actual_result);

  CompileAndFailCall(_SC("Function(invalidParameter)"));
  CheckErrorString(_SC("parameter 1 has an invalid type 'OtherBoundClass' ; expected: 'BoundClass'"));
}

//----------------------------------------------------------------------------------------------------------------------
// TestOneParameter
//----------------------------------------------------------------------------------------------------------------------
TEST_F(BindClassFunctionTest, TestOneParameter)
{
  ::testing::StrictMock<MockFunctionWithOneParameter> mock;
  MockFunctionWithOneParameter::m_instance = &mock;

  sq_pushroottable(m_vm);
  sqb::Bind::BindClass<BoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("BoundClass"));
  sqb::Bind::BindClass<OtherBoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("OtherBoundClass"));
  sq_poptop(m_vm);

  OtherBoundClass invalidParameter;
  BoundClass parameter0(0);

  BoundClass expected_result(0xffffffff);
  EXPECT_CALL(mock, ClassFunction(parameter0))
    .WillOnce(::testing::Return(expected_result));
  EXPECT_CALL(mock, ConstClassFunction(parameter0))
    .WillOnce(::testing::Return(expected_result));

  // bind the objects required for calling the function
  //
  sq_pushroottable(m_vm);

  // bind the mock class
  //
  EXPECT_SQ_SUCCEEDED(m_vm, sq_newclass(m_vm, SQFalse));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_settypetag(m_vm, -1, sqb::ClassTypeTag<MockFunctionWithOneParameter>::Get()));
  HSQOBJECT class_object;
  sq_getstackobj(m_vm, -1, &class_object);
  sqb::ClassTypeTag<MockFunctionWithOneParameter>::Get()->SetClassObject(m_vm, class_object);
  EXPECT_TRUE(sqb::Bind::BindClassFunction<MockFunctionWithOneParameter>(m_vm, -1, &MockFunctionWithOneParameter::ClassFunction, _SC("ClassFunction")));
  EXPECT_TRUE(sqb::Bind::BindClassFunction<MockFunctionWithOneParameter>(m_vm, 2, &MockFunctionWithOneParameter::ConstClassFunction, _SC("ConstClassFunction")));
  sq_poptop(m_vm);

  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, static_cast<MockFunctionWithOneParameter *>(&mock), _SC("mock")));

  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, &invalidParameter, _SC("invalidParameter")));
  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, &parameter0, _SC("parameter0")));
  sq_poptop(m_vm);

  BoundClass actual_result = CompileAndCallReturnResult<BoundClass>(_SC("return mock.ClassFunction(parameter0)"));
  EXPECT_EQ(expected_result, actual_result);
  actual_result = CompileAndCallReturnResult<BoundClass>(_SC("return mock.ConstClassFunction(parameter0)"));
  EXPECT_EQ(expected_result, actual_result);

  CompileAndFailCall(_SC("mock.ClassFunction(invalidParameter)"));
  CheckErrorString(_SC("parameter 1 has an invalid type 'OtherBoundClass' ; expected: 'BoundClass'"));
  CompileAndFailCall(_SC("mock.ConstClassFunction(invalidParameter)"));
  CheckErrorString(_SC("parameter 1 has an invalid type 'OtherBoundClass' ; expected: 'BoundClass'"));
}

//----------------------------------------------------------------------------------------------------------------------
// TestOneParameter
//----------------------------------------------------------------------------------------------------------------------
TEST_F(BindSingletonFunctionTest, TestOneParameter)
{
  ::testing::StrictMock<MockFunctionWithOneParameter> mock;
  MockFunctionWithOneParameter::m_instance = &mock;

  sq_pushroottable(m_vm);
  sqb::Bind::BindClass<BoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("BoundClass"));
  sqb::Bind::BindClass<OtherBoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("OtherBoundClass"));
  sq_poptop(m_vm);

  OtherBoundClass invalidParameter;
  BoundClass parameter0(0);

  BoundClass expected_result(0xffffffff);
  EXPECT_CALL(mock, ClassFunction(parameter0))
    .WillOnce(::testing::Return(expected_result));
  EXPECT_CALL(mock, ConstClassFunction(parameter0))
    .WillOnce(::testing::Return(expected_result));

  // bind the objects required for calling the function
  //
  sq_pushroottable(m_vm);

  // bind the mock class
  //
  sq_pushstring(m_vm, _SC("Mock"), -1);
  EXPECT_SQ_SUCCEEDED(m_vm, sq_newclass(m_vm, SQFalse));
  EXPECT_TRUE(sqb::Bind::BindSingletonFunction<MockFunctionWithOneParameter>(m_vm, -1, &mock, &MockFunctionWithOneParameter::ClassFunction, _SC("ClassFunction")));
  EXPECT_TRUE(sqb::Bind::BindSingletonFunction<MockFunctionWithOneParameter>(m_vm, 1, &mock, &MockFunctionWithOneParameter::ConstClassFunction, _SC("ConstClassFunction")));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_rawset(m_vm, -3));

  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, &invalidParameter, _SC("invalidParameter")));
  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, &parameter0, _SC("parameter0")));
  sq_poptop(m_vm);

  BoundClass actual_result = CompileAndCallReturnResult<BoundClass>(_SC("return Mock.ClassFunction(parameter0)"));
  EXPECT_EQ(expected_result, actual_result);
  actual_result = CompileAndCallReturnResult<BoundClass>(_SC("return ConstClassFunction(parameter0)"));
  EXPECT_EQ(expected_result, actual_result);

  CompileAndFailCall(_SC("Mock.ClassFunction(invalidParameter)"));
  CheckErrorString(_SC("parameter 1 has an invalid type 'OtherBoundClass' ; expected: 'BoundClass'"));
  CompileAndFailCall(_SC("ConstClassFunction(invalidParameter)"));
  CheckErrorString(_SC("parameter 1 has an invalid type 'OtherBoundClass' ; expected: 'BoundClass'"));
}

//----------------------------------------------------------------------------------------------------------------------
// TestOneParameterReturnInvalid
//----------------------------------------------------------------------------------------------------------------------
TEST_F(BindFunctionTest, TestOneParameterReturnInvalid)
{
  ::testing::StrictMock<MockFunctionWithOneParameter> mock;
  MockFunctionWithOneParameter::m_instance = &mock;

  sq_pushroottable(m_vm);
  sqb::Bind::BindClass<BoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("BoundClass"));
  sqb::Bind::BindClass<OtherBoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("OtherBoundClass"));
  sq_poptop(m_vm);

  BoundClass parameter0(0);

  InvalidStackUtilsClass expected_result;
  EXPECT_CALL(mock, ClassInvalidFunction(parameter0))
    .Times(2)
    .WillRepeatedly(::testing::Return(expected_result));

  // bind the objects required for calling the function
  //
  sq_pushroottable(m_vm);

  EXPECT_TRUE(sqb::Bind::BindFunction(m_vm, -1, &MockFunctionWithOneParameter::InvalidFunction, _SC("InvalidFunction")));

  sq_pushstring(m_vm, _SC("Mock"), -1);
  EXPECT_SQ_SUCCEEDED(m_vm, sq_newclass(m_vm, SQFalse));
  EXPECT_TRUE(sqb::Bind::BindFunction(m_vm, 3, &MockFunctionWithOneParameter::InvalidFunction, _SC("InvalidFunction")));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_rawset(m_vm, -3));

  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, &parameter0, _SC("parameter0")));
  sq_poptop(m_vm);

  CompileAndFailCall(_SC("InvalidFunction(parameter0)"));
  CheckErrorString(_SC("error returning object of type 'InvalidStackUtilsClass'"));
  CompileAndFailCall(_SC("Mock.InvalidFunction(parameter0)"));
  CheckErrorString(_SC("error returning object of type 'InvalidStackUtilsClass'"));
}

//----------------------------------------------------------------------------------------------------------------------
// TestOneParameterReturnInvalid
//----------------------------------------------------------------------------------------------------------------------
TEST_F(BindClassFunctionTest, TestOneParameterReturnInvalid)
{
  ::testing::StrictMock<MockFunctionWithOneParameter> mock;
  MockFunctionWithOneParameter::m_instance = &mock;

  sq_pushroottable(m_vm);
  sqb::Bind::BindClass<BoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("BoundClass"));
  sqb::Bind::BindClass<OtherBoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("OtherBoundClass"));
  sq_poptop(m_vm);

  BoundClass parameter0(0);

  InvalidStackUtilsClass expected_result;
  EXPECT_CALL(mock, ClassInvalidFunction(parameter0))
    .WillOnce(::testing::Return(expected_result));
  EXPECT_CALL(mock, ConstClassInvalidFunction(parameter0))
    .WillOnce(::testing::Return(expected_result));

  // bind the objects required for calling the function
  //
  sq_pushroottable(m_vm);

  // bind the mock class
  //
  EXPECT_SQ_SUCCEEDED(m_vm, sq_newclass(m_vm, SQFalse));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_settypetag(m_vm, -1, sqb::ClassTypeTag<MockFunctionWithOneParameter>::Get()));
  HSQOBJECT class_object;
  sq_getstackobj(m_vm, -1, &class_object);
  sqb::ClassTypeTag<MockFunctionWithOneParameter>::Get()->SetClassObject(m_vm, class_object);
  EXPECT_TRUE(sqb::Bind::BindClassFunction<MockFunctionWithOneParameter>(m_vm, -1, &MockFunctionWithOneParameter::ClassInvalidFunction, _SC("ClassInvalidFunction")));
  EXPECT_TRUE(sqb::Bind::BindClassFunction<MockFunctionWithOneParameter>(m_vm, 2, &MockFunctionWithOneParameter::ConstClassInvalidFunction, _SC("ConstClassInvalidFunction")));
  sq_poptop(m_vm);

  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, static_cast<MockFunctionWithOneParameter *>(&mock), _SC("mock")));

  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, &parameter0, _SC("parameter0")));
  sq_poptop(m_vm);

  CompileAndFailCall(_SC("mock.ClassInvalidFunction(parameter0)"));
  CheckErrorString(_SC("error returning object of type 'InvalidStackUtilsClass'"));
  CompileAndFailCall(_SC("mock.ConstClassInvalidFunction(parameter0)"));
  CheckErrorString(_SC("error returning object of type 'InvalidStackUtilsClass'"));
}

//----------------------------------------------------------------------------------------------------------------------
// TestOneParameterReturnInvalid
//----------------------------------------------------------------------------------------------------------------------
TEST_F(BindSingletonFunctionTest, TestOneParameterReturnInvalid)
{
  ::testing::StrictMock<MockFunctionWithOneParameter> mock;
  MockFunctionWithOneParameter::m_instance = &mock;

  sq_pushroottable(m_vm);
  sqb::Bind::BindClass<BoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("BoundClass"));
  sqb::Bind::BindClass<OtherBoundClass, sqb::NoBaseClass>(m_vm, -1, _SC("OtherBoundClass"));
  sq_poptop(m_vm);

  BoundClass parameter0(0);

  InvalidStackUtilsClass expected_result;
  EXPECT_CALL(mock, ClassInvalidFunction(parameter0))
    .WillOnce(::testing::Return(expected_result));
  EXPECT_CALL(mock, ConstClassInvalidFunction(parameter0))
    .WillOnce(::testing::Return(expected_result));

  // bind the objects required for calling the function
  //
  sq_pushroottable(m_vm);

  // bind the mock class
  //
  sq_pushstring(m_vm, _SC("Mock"), -1);
  EXPECT_SQ_SUCCEEDED(m_vm, sq_newclass(m_vm, SQFalse));
  EXPECT_TRUE(sqb::Bind::BindSingletonFunction<MockFunctionWithOneParameter>(m_vm, -1, &mock, &MockFunctionWithOneParameter::ClassInvalidFunction, _SC("ClassInvalidFunction")));
  EXPECT_TRUE(sqb::Bind::BindSingletonFunction<MockFunctionWithOneParameter>(m_vm, 1, &mock, &MockFunctionWithOneParameter::ConstClassInvalidFunction, _SC("ConstClassInvalidFunction")));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_rawset(m_vm, -3));

  EXPECT_TRUE(sqb::Bind::BindVariable(m_vm, -1, &parameter0, _SC("parameter0")));
  sq_poptop(m_vm);

  CompileAndFailCall(_SC("Mock.ClassInvalidFunction(parameter0)"));
  CheckErrorString(_SC("error returning object of type 'InvalidStackUtilsClass'"));
  CompileAndFailCall(_SC("ConstClassInvalidFunction(parameter0)"));
  CheckErrorString(_SC("error returning object of type 'InvalidStackUtilsClass'"));
}
