#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "gtest/gtest.h"

#include <squirrel.h>

#include <sqbind/sqbClassTypeTag.h>

#include "fixtures/SquirrelFixture.h"
#include "StringHelpers.h"
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
/// \brief Base squirrel fixture that can generate types.
//----------------------------------------------------------------------------------------------------------------------
template<typename TypeParam>
class BaseTypeFixture : public SquirrelFixture
{
public:
  virtual void SetUp() override;
  virtual void TearDown() override;

  TypeParam GetTypeFromStack(SQInteger index);

  TypeParam GetRandomValue();

  TypeParam CompileAndCallReturnResult(const SQChar* buffer);

protected:
  testing::AssertionResult CmpHelperTypeParamEQ(
    const char* expected_expression,
    const char* actual_expression,
    TypeParam expected,
    TypeParam actual);
};

/// \brief Macro for checking TypeParam values are equal.
#define EXPECT_TYPE_PARAM_EQ(expected, actual) \
  EXPECT_PRED_FORMAT2(BaseTypeFixture<TypeParam>::CmpHelperTypeParamEQ, expected, actual)

#include "TypeFixture.inl"
