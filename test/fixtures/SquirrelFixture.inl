#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
// SquirrelFixture
//----------------------------------------------------------------------------------------------------------------------
template<typename TypeParam>
inline TypeParam SquirrelFixture::GetTypeFromStack(SQInteger index)
{
  SQInteger value;
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getinteger(m_vm, index, &value));
  return static_cast<TypeParam>(value);
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline bool SquirrelFixture::GetTypeFromStack<bool>(SQInteger index)
{
  SQBool value;
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getbool(m_vm, index, &value));
  return (value == SQTrue);
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline float SquirrelFixture::GetTypeFromStack<float>(SQInteger index)
{
  SQFloat value;
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getfloat(m_vm, index, &value));
  return static_cast<float>(value);
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline const SQChar* SquirrelFixture::GetTypeFromStack<const SQChar*>(SQInteger index)
{
  const SQChar *value;
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, index, &value));
  return value;
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline SQUserPointer SquirrelFixture::GetTypeFromStack<SQUserPointer>(SQInteger index)
{
  SQUserPointer value;
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getuserpointer(m_vm, index, &value));
  return value;
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline BoundClass SquirrelFixture::GetTypeFromStack<BoundClass>(SQInteger index)
{
  sqb::ClassTypeTagBase* classTypeTag = sqb::ClassTypeTag<BoundClass>::Get();
  SQUserPointer value = nullptr;
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getinstanceup(m_vm, index, &value, classTypeTag));
  BoundClass result(0);
  if (value != nullptr)
  {
    result = *static_cast<BoundClass*>(value);
  }
  return result;
}

#if defined(SQUSEDOUBLE)
//----------------------------------------------------------------------------------------------------------------------
template<>
inline double SquirrelFixture::GetTypeFromStack<double>(SQInteger index)
{
  SQFloat value;
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getfloat(m_vm, index, &value));
  return static_cast<double>(value);
}
#endif // defined(SQUSEDOUBLE)

//----------------------------------------------------------------------------------------------------------------------
template<typename TypeParam>
TypeParam SquirrelFixture::CompileAndCallReturnResult(const SQChar* buffer)
{
  EXPECT_SQ_SUCCEEDED(m_vm, sq_compilebuffer(m_vm, buffer, scstrlen(buffer), _SC("buffer"), SQFalse));
  sq_pushroottable(m_vm);
  EXPECT_SQ_SUCCEEDED(m_vm, sq_call(m_vm, 1, SQTrue, SQFalse));
  TypeParam result = GetTypeFromStack<TypeParam>(-1);
  if (!sq_isnull(m_returnResultReference))
  {
    sq_release(m_vm, &m_returnResultReference);
  }
  sq_getstackobj(m_vm, -1, &m_returnResultReference);
  sq_addref(m_vm, &m_returnResultReference);
  sq_pop(m_vm, 2);

  return result;
}
