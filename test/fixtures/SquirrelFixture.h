#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "gtest/gtest.h"

#include <squirrel.h>

#include <sqbind/sqbAssert.h>
#include <sqbind/sqbClassTypeTag.h>

#include "../TypeHelpers.h"
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
/// \brief This is a base test fixture that simply initialises a squirrel vm.
/// The fixture will test that the stack size starts and ends the same so make sure all tests leave nothing
/// on the stack.
//----------------------------------------------------------------------------------------------------------------------
class SquirrelFixture : public ::testing::Test
{
protected:
  /// \brief The squirrel vm to be used by the test.
  HSQUIRRELVM m_vm;
  /// \brief This will be the starting value of the stack.
  SQInteger m_top;

  virtual ~SquirrelFixture();

  virtual void SetUp() override;
  virtual void TearDown() override;

  template<typename TypeParam>
  TypeParam GetTypeFromStack(SQInteger index);

  void CompileAndSucceedCall(const SQChar *buffer);
  void CompileAndFailCall(const SQChar *buffer);
  template<typename TypeParam>
  TypeParam CompileAndCallReturnResult(const SQChar* buffer);

  void CheckErrorString(const SQChar *expectedErrorFormatString, ...);

private:
  HSQOBJECT m_returnResultReference;
};

#include "SquirrelFixture.inl"
