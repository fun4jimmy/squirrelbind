//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "SquirrelVMFixture.h"
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
// SquirrelVMFixture
//----------------------------------------------------------------------------------------------------------------------
SQObjectType SquirrelVMFixture::GetGlobalObjectType(HSQUIRRELVM vm, const SQChar *functionName) const
{
  sq_pushroottable(vm);
  sq_pushstring(vm, functionName, -1);
  if (SQ_SUCCEEDED(sq_rawget(vm, -2)))
  {
    SQObjectType type = sq_gettype(vm, -1);
    sq_pop(vm, 2);
    return type;
  }
  sq_poptop(vm);
  return OT_NULL;
}

//----------------------------------------------------------------------------------------------------------------------
bool SquirrelVMFixture::IsStdBlobLibRegistered(HSQUIRRELVM vm) const
{
  bool registered = GetGlobalObjectType(vm, _SC("castf2i")) != OT_NULL;
  return registered;
}

//----------------------------------------------------------------------------------------------------------------------
bool SquirrelVMFixture::IsStdIOLibRegistered(HSQUIRRELVM vm) const
{
  bool registered = GetGlobalObjectType(vm, _SC("dofile")) != OT_NULL;
  return registered;
}

//----------------------------------------------------------------------------------------------------------------------
bool SquirrelVMFixture::IsStdMathLibRegistered(HSQUIRRELVM vm) const
{
  bool registered = GetGlobalObjectType(vm, _SC("abs")) != OT_NULL;
  return registered;
}

//----------------------------------------------------------------------------------------------------------------------
bool SquirrelVMFixture::IsStdStringLibRegistered(HSQUIRRELVM vm) const
{
  bool registered = GetGlobalObjectType(vm, _SC("format")) != OT_NULL;
  return registered;
}

//----------------------------------------------------------------------------------------------------------------------
bool SquirrelVMFixture::IsStdSystemLibRegistered(HSQUIRRELVM vm) const
{
  bool registered = GetGlobalObjectType(vm, _SC("clock")) != OT_NULL;
  return registered;
}

//----------------------------------------------------------------------------------------------------------------------

