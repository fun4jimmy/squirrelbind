#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
template<template <typename, typename> class ClassDefinitionType>
void ClassDefinitionFixture<ClassDefinitionType>::SetUp()
{
  using namespace NAMESPACE_NAME;
  
  SquirrelFixture::SetUp();

  SQInteger top = sq_gettop(m_vm);

  sq_pushroottable(m_vm);

  BaseClassDefinitionType(m_vm, -1, _SC("MockBaseClass"))
    .EnumEntry(MockBaseClass::kBaseEnumValue, _SC("kBaseEnumValue"))
    .Variable(&MockBaseClass::m_baseInstanceValue, _SC("m_baseInstanceValue"))
    .StaticVariable(MockBaseClass::m_baseStaticValue, _SC("m_baseStaticValue"))
    .ClassFunction(&MockBaseClass::BaseClassFunction, _SC("BaseClassFunction"))
    .ClassFunction(&MockBaseClass::BaseConstClassFunction, _SC("BaseConstClassFunction"))
    .NativeClassFunction(&MockBaseClass::BaseNativeClassFunction, _SC("BaseNativeClassFunction"))
    .Function(&MockBaseClass::BaseFunction, _SC("BaseFunction"))
    .NativeFunction(&MockBaseClass::BaseNativeFunction, _SC("BaseNativeFunction"))
    .SingletonFunction(
      &m_mockBaseClass,
      &MockBaseClass::BaseClassFunction,
      _SC("BaseSingletonFunction"))
    .SingletonFunction(
      &m_mockBaseClass,
      &MockBaseClass::BaseConstClassFunction,
      _SC("BaseConstSingletonFunction"))
    .NativeSingletonFunction(
      &m_mockBaseClass,
      &MockBaseClass::BaseNativeClassFunction,
      _SC("BaseNativeSingletonFunction")
    );

  sq_pushstring(m_vm, _SC("m_mockBaseClass"), -1);
  EXPECT_SQ_SUCCEEDED(m_vm, sqb::Push<MockBaseClass>(m_vm, &m_mockBaseClass));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_newslot(m_vm, -3, SQFalse));

  DerivedClassNoOffsetDefinitionType(m_vm, -1, _SC("MockDerivedClassNoOffset"))
    .EnumEntry(MockDerivedClassNoOffset::kDerivedEnumValue, _SC("kDerivedEnumValue"))
    .Variable(&MockDerivedClassNoOffset::m_derivedInstanceValue, _SC("m_derivedInstanceValue"))
    .StaticVariable(MockDerivedClassNoOffset::m_derivedStaticValue, _SC("m_derivedStaticValue"))
    .ClassFunction(&MockDerivedClassNoOffset::DerivedClassFunction, _SC("DerivedClassFunction"))
    .ClassFunction(&MockDerivedClassNoOffset::DerivedConstClassFunction, _SC("DerivedConstClassFunction"))
    .NativeClassFunction(&MockDerivedClassNoOffset::DerivedNativeClassFunction, _SC("DerivedNativeClassFunction"))
    .Function(&MockDerivedClassNoOffset::DerivedFunction, _SC("DerivedFunction"))
    .NativeFunction(&MockDerivedClassNoOffset::DerivedNativeFunction, _SC("DerivedNativeFunction"))
    .SingletonFunction(
      &m_mockDerivedClassNoOffset,
      &MockDerivedClassNoOffset::DerivedClassFunction,
      _SC("DerivedSingletonFunction"))
    .SingletonFunction(
      &m_mockDerivedClassNoOffset,
      &MockDerivedClassNoOffset::DerivedConstClassFunction,
      _SC("DerivedConstSingletonFunction"))
    .NativeSingletonFunction(
      &m_mockDerivedClassNoOffset,
      &MockDerivedClassNoOffset::DerivedNativeClassFunction,
      _SC("DerivedNativeSingletonFunction")
    );

  sq_pushstring(m_vm, _SC("m_mockDerivedClassNoOffset"), -1);
  EXPECT_SQ_SUCCEEDED(m_vm, sqb::Push<MockDerivedClassNoOffset>(m_vm, &m_mockDerivedClassNoOffset));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_rawset(m_vm, -3));

  DerivedClassWithOffsetDefinitionType(m_vm, -1, _SC("MockDerivedClassWithOffset"))
    .EnumEntry(MockDerivedClassWithOffset::kDerivedEnumValue, _SC("kDerivedEnumValue"))
    .Variable(&MockDerivedClassWithOffset::m_derivedInstanceValue, _SC("m_derivedInstanceValue"))
    .StaticVariable(MockDerivedClassWithOffset::m_derivedStaticValue, _SC("m_derivedStaticValue"))
    .ClassFunction(&MockDerivedClassWithOffset::DerivedClassFunction, _SC("DerivedClassFunction"))
    .ClassFunction(&MockDerivedClassWithOffset::DerivedConstClassFunction, _SC("DerivedConstClassFunction"))
    .NativeClassFunction(&MockDerivedClassWithOffset::DerivedNativeClassFunction, _SC("DerivedNativeClassFunction"))
    .Function(&MockDerivedClassWithOffset::DerivedFunction, _SC("DerivedFunction"))
    .NativeFunction(&MockDerivedClassWithOffset::DerivedNativeFunction, _SC("DerivedNativeFunction"))
    .SingletonFunction(
      &m_mockDerivedClassWithOffset,
      &MockDerivedClassWithOffset::DerivedClassFunction,
      _SC("DerivedSingletonFunction"))
    .SingletonFunction(
      &m_mockDerivedClassWithOffset,
      &MockDerivedClassWithOffset::DerivedConstClassFunction,
      _SC("DerivedConstSingletonFunction"))
    .NativeSingletonFunction(
      &m_mockDerivedClassWithOffset,
      &MockDerivedClassWithOffset::DerivedNativeClassFunction,
      _SC("DerivedNativeSingletonFunction")
    );

  sq_pushstring(m_vm, _SC("m_mockDerivedClassWithOffset"), -1);
  EXPECT_SQ_SUCCEEDED(m_vm, sqb::Push<MockDerivedClassWithOffset>(m_vm, &m_mockDerivedClassWithOffset));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_rawset(m_vm, -3));

#if SUPPORTS_ALIGNMENT
  AlignedClassDefinitionType(m_vm, -1, _SC("AlignedClass"));
#endif

  sq_poptop(m_vm);

  EXPECT_EQ(top, sq_gettop(m_vm));
}
