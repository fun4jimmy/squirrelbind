//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "SquirrelFixture.h"

#include <stdarg.h>
#include <stdio.h>
//----------------------------------------------------------------------------------------------------------------------

#if defined(SQBIND_COMPILER_MSVC)
# if defined(SQUNICODE)
#  define scvscprintf _vscwprintf
#  define scvsnprintf _vsnwprintf_s
# else // defined(SQUNICODE)
#  define scvscprintf _vscprintf
#  define scvsnprintf vsnprintf_s
# endif // defined(SQUNICODE)
#else
# if !defined(SQUNICODE)
#  define scvsnprintf vsnprintf
# endif // !defined(SQUNICODE)
#endif // defined(SQBIND_COMPILER_MSVC)

//----------------------------------------------------------------------------------------------------------------------
/// SquirrelFixture
//----------------------------------------------------------------------------------------------------------------------
SquirrelFixture::~SquirrelFixture()
{
}

//----------------------------------------------------------------------------------------------------------------------
void SquirrelFixture::SetUp()
{
  m_vm = sq_open(1024);

  m_top = sq_gettop(m_vm);

  sq_resetobject(&m_returnResultReference);
}

//----------------------------------------------------------------------------------------------------------------------
void SquirrelFixture::TearDown()
{
  if (!sq_isnull(m_returnResultReference))
  {
    sq_release(m_vm, &m_returnResultReference);
  }

  SQInteger top = sq_gettop(m_vm);

  EXPECT_EQ(m_top, top);

  sq_close(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
void SquirrelFixture::CompileAndSucceedCall(const SQChar *buffer)
{
  ASSERT_SQ_SUCCEEDED(m_vm, sq_compilebuffer(m_vm, buffer, scstrlen(buffer), _SC("buffer"), SQFalse));
  sq_pushroottable(m_vm);
  ASSERT_SQ_SUCCEEDED(m_vm, sq_call(m_vm, 1, SQFalse, SQFalse));
  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
void SquirrelFixture::CompileAndFailCall(const SQChar *buffer)
{
  ASSERT_SQ_SUCCEEDED(m_vm, sq_compilebuffer(m_vm, buffer, scstrlen(buffer), _SC("buffer"), SQFalse));
  sq_pushroottable(m_vm);
  EXPECT_SQ_FAILED(sq_call(m_vm, 1, SQFalse, SQFalse));
  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
void SquirrelFixture::CheckErrorString(const SQChar *expectedErrorFormatString, ...)
{
#if defined(SQBIND_COMPILER_MSVC)
  va_list vargs;
  va_start(vargs, expectedErrorFormatString);
  int length = scvscprintf(expectedErrorFormatString, vargs) + 1;
  va_end(vargs);

  SQChar *expectedErrorString = sq_getscratchpad(m_vm, length * sizeof(SQChar));

  va_start(vargs, expectedErrorFormatString);
  scvsnprintf(expectedErrorString, length, length - 1, expectedErrorFormatString, vargs);
  va_end(vargs);
#else // defined(SQBIND_COMPILER_MSVC)
  va_list vargs1;
  va_start(vargs1, expectedErrorFormatString);
  va_list vargs2;
  va_copy(vargs2, vargs1);

  const int length = scvsnprintf(nullptr, 0, expectedErrorFormatString, vargs1);
  va_end(vargs1);
  
  SQChar *expectedErrorString = sq_getscratchpad(m_vm, (length + 1) * sizeof(SQChar));

  scvsnprintf(expectedErrorString, (length + 1), expectedErrorFormatString, vargs2);
  va_end(vargs2);
#endif // defined(SQBIND_COMPILER_MSVC)

  sq_getlasterror(m_vm);
  EXPECT_EQ(OT_STRING, sq_gettype(m_vm, -1));
  const SQChar *actualErrorString;
  EXPECT_SQ_SUCCEEDED(m_vm, sq_getstring(m_vm, -1, &actualErrorString));
  EXPECT_STREQ(expectedErrorString, actualErrorString);
  sq_poptop(m_vm);
}
