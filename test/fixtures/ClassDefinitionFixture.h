#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "gtest/gtest.h"

#include <sqbind/sqbClassDefinition.h>

#include "fixtures/SquirrelFixture.h"
//----------------------------------------------------------------------------------------------------------------------

#if !defined(NAMESPACE_NAME)
#error
#endif

#if !defined(CLASS_DEFINITION_TYPE)
#error
#endif

#if !defined(TEST_NAME)
#error
#endif

//----------------------------------------------------------------------------------------------------------------------
template<template <typename, typename> class ClassDefinitionType>
class ClassDefinitionFixture : public SquirrelFixture
{
public:
  typedef ClassDefinitionType<NAMESPACE_NAME::MockBaseClass, sqb::NoBaseClass> BaseClassDefinitionType;
  typedef ClassDefinitionType<NAMESPACE_NAME::MockDerivedClassNoOffset, NAMESPACE_NAME::MockBaseClass> DerivedClassNoOffsetDefinitionType;
  typedef ClassDefinitionType<NAMESPACE_NAME::MockDerivedClassWithOffset, NAMESPACE_NAME::MockBaseClass> DerivedClassWithOffsetDefinitionType;
  typedef ClassDefinitionType<NAMESPACE_NAME::AlignedClass, sqb::NoBaseClass> AlignedClassDefinitionType;

  testing::StrictMock<NAMESPACE_NAME::MockBaseClass> m_mockBaseClass;
  testing::StrictMock<NAMESPACE_NAME::MockDerivedClassNoOffset> m_mockDerivedClassNoOffset;
  testing::StrictMock<NAMESPACE_NAME::MockDerivedClassWithOffset> m_mockDerivedClassWithOffset;

  void SetUp();
};

typedef ClassDefinitionFixture<CLASS_DEFINITION_TYPE> TEST_NAME;

#include "ClassDefinitionFixture.inl"
