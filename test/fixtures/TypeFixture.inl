#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
// BaseTypeFixture
//----------------------------------------------------------------------------------------------------------------------
template<typename TypeParam>
inline void BaseTypeFixture<TypeParam>::SetUp()
{
  SquirrelFixture::SetUp();

  // setup the BoundClass class
  //
  EXPECT_SQ_SUCCEEDED(m_vm, sq_newclass(m_vm, SQFalse));
  EXPECT_SQ_SUCCEEDED(m_vm, sq_settypetag(m_vm, -1, sqb::ClassTypeTag<BoundClass>::Get()));
  HSQOBJECT class_object;
  sq_getstackobj(m_vm, -1, &class_object);
  sqb::ClassTypeTag<BoundClass>::Get()->SetClassObject(m_vm, class_object);
  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
template<typename TypeParam>
inline void BaseTypeFixture<TypeParam>::TearDown()
{
  SquirrelFixture::TearDown();
}

//----------------------------------------------------------------------------------------------------------------------
template<typename TypeParam>
TypeParam BaseTypeFixture<TypeParam>::GetTypeFromStack(SQInteger index)
{
  return SquirrelFixture::GetTypeFromStack<TypeParam>(index);
}

//----------------------------------------------------------------------------------------------------------------------
template<typename TypeParam>
inline TypeParam BaseTypeFixture<TypeParam>::GetRandomValue()
{
  // use a double so we keep maximum amount of precision until it is absolutely necessary to convert.
  //
  double random_scale = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
  double minimum = std::numeric_limits<TypeParam>::min();
  double maximum = std::numeric_limits<TypeParam>::max();
  const double value = (random_scale * (maximum - minimum)) + minimum;
  return static_cast<TypeParam>(value);
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline bool BaseTypeFixture<bool>::GetRandomValue()
{
  bool value = (rand() % 2) == 1;
  return value;
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline const SQChar *BaseTypeFixture<const SQChar *>::GetRandomValue()
{
  const SQChar *value = GetRandomString();
  return value;
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline SQUserPointer BaseTypeFixture<SQUserPointer>::GetRandomValue()
{
  int randomNumber = rand();
  SQUserPointer value = reinterpret_cast<SQUserPointer>(randomNumber);
  return value;
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline BoundClass BaseTypeFixture<BoundClass>::GetRandomValue()
{
  // use a double so we keep maximum amount of precision until it is absolutely necessary to convert.
  //
  double random_scale = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
  double minimum = std::numeric_limits<uint32_t>::min();
  double maximum = std::numeric_limits<uint32_t>::max();
  uint32_t value = static_cast<uint32_t>((random_scale * (maximum - minimum)) + minimum);
  BoundClass result(value);
  return result;
}

#if defined(_SQ64)
//----------------------------------------------------------------------------------------------------------------------
template<>
inline int64_t BaseTypeFixture<int64_t>::GetRandomValue()
{
  int64_t value = rand() * rand();
  if (rand() % 2 == 0)
  {
    value = -value;
  }
  return value;
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline uint64_t BaseTypeFixture<uint64_t>::GetRandomValue()
{
  int64_t value = rand() * rand();
  return value;
}
#endif

//----------------------------------------------------------------------------------------------------------------------
template<>
inline TestEnum BaseTypeFixture<TestEnum>::GetRandomValue()
{
  TestEnum value = static_cast<TestEnum>(rand() % kTestEnumCount);
  return value;
}

//----------------------------------------------------------------------------------------------------------------------
template<typename TypeParam>
inline testing::AssertionResult BaseTypeFixture<TypeParam>::CmpHelperTypeParamEQ(
  const char* expected_expression,
  const char* actual_expression,
  TypeParam expected,
  TypeParam actual)
{
  return testing::internal::EqHelper<GTEST_IS_NULL_LITERAL_(expected)>::template Compare<TypeParam, TypeParam>(
    expected_expression,
    actual_expression,
    expected,
    actual);
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline testing::AssertionResult BaseTypeFixture<float>::CmpHelperTypeParamEQ(
  const char* expected_expression,
  const char* actual_expression,
  float expected,
  float actual)
{
  return testing::internal::CmpHelperFloatingPointEQ<float>(
    expected_expression,
    actual_expression,
    expected,
    actual);
}

//----------------------------------------------------------------------------------------------------------------------
template<>
inline testing::AssertionResult BaseTypeFixture<const SQChar *>::CmpHelperTypeParamEQ(
  const char* expected_expression,
  const char* actual_expression,
  const SQChar* expected,
  const SQChar* actual)
{
  return testing::internal::CmpHelperSTREQ(
    expected_expression,
    actual_expression,
    expected,
    actual);
}

#if defined(SQUSEDOUBLE)
//----------------------------------------------------------------------------------------------------------------------
template<>
inline testing::AssertionResult BaseTypeFixture<double>::CmpHelperTypeParamEQ(
  const char* expected_expression,
  const char* actual_expression,
  double expected,
  double actual)
{
  return testing::internal::CmpHelperFloatingPointEQ<double>(
    expected_expression,
    actual_expression,
    expected,
    actual);
}
#endif

//----------------------------------------------------------------------------------------------------------------------
template<typename TypeParam>
TypeParam BaseTypeFixture<TypeParam>::CompileAndCallReturnResult(const SQChar* buffer)
{
  return SquirrelFixture::CompileAndCallReturnResult<TypeParam>(buffer);
}
