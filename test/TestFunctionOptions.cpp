//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2012 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <gmock/gmock.h>

#include <sqbind/sqbFunctionOptions.h>
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
TEST(FunctionOptionsTest, TestFreeVariables)
{
  sqb::FunctionOptions options;

  EXPECT_EQ(0u, options.m_freeVariableCount);

  options.FreeVariables(10);
  EXPECT_EQ(10u, options.m_freeVariableCount);

  options.FreeVariables(5);
  EXPECT_EQ(5u, options.m_freeVariableCount);
}

//----------------------------------------------------------------------------------------------------------------------
TEST(FunctionOptionsTest, TestParamCheckCount)
{
  sqb::FunctionOptions options;

  EXPECT_EQ(0, options.m_paramCheckCount);

  options.ParamCheckCount(10);
  EXPECT_EQ(10, options.m_paramCheckCount);

  options.ParamCheckCount(5);
  EXPECT_EQ(5, options.m_paramCheckCount);
}

//----------------------------------------------------------------------------------------------------------------------
TEST(FunctionOptionsTest, TestTypeMask)
{
  sqb::FunctionOptions options;

  EXPECT_TRUE(options.m_typeMaskString == nullptr);

  options.TypeMask(_SC("."));
  EXPECT_STREQ(_SC("."), options.m_typeMaskString);

  options.TypeMask(_SC(".nn"));
  EXPECT_STREQ(_SC(".nn"), options.m_typeMaskString);
}

//----------------------------------------------------------------------------------------------------------------------
TEST(FunctionOptionsTest, TestStatic)
{
  sqb::FunctionOptions options;

  EXPECT_EQ(sqb::kNonStaticFunctionType, options.m_staticType);

  options.Static(sqb::kStaticFunctionType);
  EXPECT_EQ(sqb::kStaticFunctionType, options.m_staticType);

  options.Static(sqb::kNonStaticFunctionType);
  EXPECT_EQ(sqb::kNonStaticFunctionType, options.m_staticType);
}

//----------------------------------------------------------------------------------------------------------------------
TEST(FunctionOptionsTest, TestChainingCalls)
{
  sqb::FunctionOptions options;

  options
    .FreeVariables(7)
    .ParamCheckCount(2)
    .TypeMask(_SC(".x"))
    .Static(sqb::kStaticFunctionType);

  EXPECT_EQ(7u, options.m_freeVariableCount);
  EXPECT_EQ(2, options.m_paramCheckCount);
  EXPECT_STREQ(_SC(".x"), options.m_typeMaskString);
  EXPECT_EQ(sqb::kStaticFunctionType, options.m_staticType);
}
